const formatTime = date => {
  const year = date.getFullYear()
  const month = date.getMonth() + 1
  const day = date.getDate()
  const hour = date.getHours()
  const minute = date.getMinutes()
  const second = date.getSeconds()

  return [year, month, day].map(formatNumber).join('/') + ' ' + [hour, minute, second].map(formatNumber).join(':')
}

const formatNumber = n => {
  n = n.toString()
  return n[1] ? n : '0' + n
}

function number_in_obj_toF2(obj) {
  for (var key in obj) {
    if (obj[key] == null) {
      obj[key] = ''
    }
    else if (typeof (obj[key]) == 'number') {
      obj[key] = parseFloat(obj[key].toFixed(2))
    }
  }
}

function number_in_objOfList_toF2(list) {
  var newList = new Array();
  if (list) {
    for (var i = 0; i < list.length; i++) {
      newList.push(number_in_obj_toF2(list[i]))
    }
  }
  return newList;
}

module.exports = {
  formatTime: formatTime,
  number_in_obj_toF2: number_in_obj_toF2,
  number_in_objOfList_toF2: number_in_objOfList_toF2
}
