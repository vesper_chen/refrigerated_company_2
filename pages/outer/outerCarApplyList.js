// pages/outer/outerCarApplyList.js

const app = getApp()
var webhost = app.globalData.webhost;
import Toast from '../Vant/toast/toast';
import utils from '../../utils/date.js';
var listData, getCodeList;

Page({
  data: {
    dataList: [],
    selectedIndex: -1,
    page: 1,
    startDate: '',
    endDate: '',
    stateNameList: [],
    stateIndex: 0,
    inputVal: '',
    inputRouteVal: '',
    loading: 0,
    inputShowed: false,
    inputRouteShowed: false,
    outerId: ''
  },

  //发货客户searchBar的js方法
  showInput: function () {
    this.setData({
      inputShowed: true,
      inputRouteShowed: false
    });
  },
  hideInput: function () {
    this.setData({
      selectedIndex: -1,
      page: 1,
      loading: 1,
      dataList: [],
      inputVal: "",
      inputShowed: false
    });
    listData();
  },
  clearInput: function () {
    this.setData({
      inputVal: ""
    });
  },
  inputTyping: function (e) {
    this.setData({
      inputVal: e.detail.value
    });
  },

  //路线searchBar的js方法
  showRouteInput: function () {
    this.setData({
      inputRouteShowed: true,
      inputShowed: false,
    });
  },
  hideRouteInput: function () {
    this.setData({
      selectedIndex: -1,
      page: 1,
      loading: 1,
      dataList: [],
      inputRouteVal: "",
      inputRouteShowed: false
    });
    listData();
  },
  clearRouteInput: function () {
    this.setData({
      inputRouteVal: ""
    });
  },
  inputRouteTyping: function (e) {
    this.setData({
      inputRouteVal: e.detail.value
    });
  },


  //输入查询条件后进行搜索
  doSearch: function(){
    var that = this;
    that.setData({
      page: 1,
      loading: 0,
      selectedIndex: -1,
      selectedState: '',
      dataList: []
    })
    listData();
  },
  

  viewClick(e) {
    var index = +e.currentTarget.dataset.id;
    var state = e.currentTarget.dataset.state;
    var outerId = e.currentTarget.dataset.outerid;
    this.setData({
      selectedIndex: index,
      selectedState: state,
      outerId: outerId
    })
  },

  goToDetail(e) {
    var outerId = e.currentTarget.dataset.outerid;
    this.setData({
      outerId: outerId
    })
    wx.navigateTo({
      url: './outerDetail?outerId=' + this.data.outerId,
    })
  },

  add: function () {
    wx.navigateTo({
      url: './addOuter',
    })
  },

  copy: function () {
    wx.navigateTo({
      url: './copyOuter?outerId=' + this.data.outerId,
    })
  },

  bindDate: function (e) {
    var that = this;
    var date = e.detail.value;
    that.setData({
      page: 1,
      loading: 0,
      selectedIndex: -1,
      selectedState: '',
      dataList: [],
      startDate: date
    })
    listData();
  },

  bindDate2: function (e) {
    var that = this;
    var date = e.detail.value;
    that.setData({
      page: 1,
      loading: 0,
      selectedIndex: -1,
      selectedState: '',
      dataList: [],
      endDate: date
    })
    listData();
  },

  bindState: function (e) {
    var that = this;
    var index = +e.detail.value;
    that.setData({
      page: 1,
      loading: 0,
      selectedIndex: -1,
      selectedState: '',
      dataList: [],
      stateIndex: index
    })
    listData();
  },

  onLoad: function () {
    console.log(app.globalData)
    var that = this;
    var myDate = new Date();
    var myDate2 = new Date(myDate.getTime() - 30 * 24 * 3600 * 1000);
    var date = utils.Format('yyyy-MM-dd', myDate);
    var date2 = utils.Format('yyyy-MM-dd', myDate2);
    that.setData({
      startDate: date2,
      endDate: date,
      date: date
    })

    listData = function () {
      wx.showNavigationBarLoading();
      var data = [{
        String: {
          sessionid: app.globalData.sessionid,
          a1: that.data.startDate,
          a2: that.data.endDate,
          a3: that.data.inputVal,
          a4: that.data.inputRouteVal,
          a5: that.data.stateCodeList[that.data.stateIndex],
          pageNumber: that.data.page,
          pageSize: 5
        }
      }]
      wx.request({
        url: webhost,
        data: {
          eap_username: app.globalData.account,
          eap_password: app.globalData.password,
          boId: "tspdriver_Tsp2BusinessServiceBO_bo", //调用的bo
          methodName: 'queryOuterCarApplyList', //调用的具体方法
          returnType: "json", //返回参数类型
          parameters: JSON.stringify(data)
        },
        header: {
          'content-type': 'application/x-www-form-urlencoded'
        },
        method: 'POST',
        complete() {
          wx.hideNavigationBarLoading();
        },
        success({
          data
        }) {
          if (data.code && data.code == '000000') {
            console.log(data)
            var list = data.dataList;
            for(var i =0; i<list.length; i++){
              list[i].a11 = (list[i].a11 * 100).toFixed(2) + '%'
            }
            if (list.length > 0) {
              console.log('有数据')
              that.setData({
                loading: 0,
                dataList: that.data.dataList.concat(list)
              })
            } else {
              console.log('无数据')
              that.setData({
                loading: 2
              })
            }
          } else if (data.code == '003000') {
            wx.showToast({
              title: data.msg,
              icon: 'none'
            })
            setTimeout(function () {
              wx.redirectTo({
                url: '../login/login',
              })
            }, 1200);
          }
        },
        fail: function (res) {
          wx.showToast({
            title: '网络连接失败',
            icon: 'none'
          })
        }
      })
    }

    getCodeList = function () {
      wx.showNavigationBarLoading();
      var data = [{
        String: {
          codeListKey: 'global_status',
        }
      }]
      wx.request({
        url: webhost,
        data: {
          eap_username: app.globalData.account,
          eap_password: app.globalData.password,
          boId: "tspdriver_Tsp2BusinessServiceBO_bo", //调用的bo
          methodName: 'getCodeList', //调用的具体方法
          returnType: "json", //返回参数类型
          parameters: JSON.stringify(data)
        },
        header: {
          'content-type': 'application/x-www-form-urlencoded'
        },
        method: 'POST',
        success: function (res) {
          var code = res.data.code;
          var nameList = ['全部'];
          var codeList = [''];
          if (code == '000000') {
            console.log('huoqumabiao')
            for (var i = 0; i < res.data.codeList.length; i++) {
              nameList[i+1] = res.data.codeList[i].codeName
              codeList[i+1] = res.data.codeList[i].codeValue
            }
            that.setData({
              stateNameList: nameList,
              stateCodeList: codeList
            })
          } else {
            wx.showModal({
              title: '提示',
              showCancel: false,
              confirmColor: '#118EDE',
              content: res.data.msg,
              success: function (res) {
                if (res.confirm) { }
              }
            })
          }
        },
        fail: function (res) {
          wx.showModal({
            title: '提示',
            showCancel: false,
            confirmColor: '#118EDE',
            content: '服务器异常',
            success: function (res) {
              if (res.confirm) { }
            }
          })
        }
      })
    }

    getCodeList();

    

  },

  onPullDownRefresh() {
    var that = this;
    setTimeout(function () {
      that.setData({
        page: 1,
        loading: 0,
        selectedIndex: -1,
        selectedState: '',
        dataList: []
      });
      listData();
      wx.stopPullDownRefresh()
    }, 2000)
  },

  onShareAppMessage() {

  },

  onShow() {
    var that = this;
    setTimeout(function () {
      that.setData({
        page: 1,
        dataList: [],
        selectedIndex: -1
      })
      listData();
    }, 500)
  },

  onReachBottom() {
    var that = this;
    console.log(that.data.loading);
    if (that.data.loading == 0) {
      that.setData({
        loading: true,
        page: that.data.page + 1
      })
      setTimeout(() => {
        listData();
      }, 300);
    }
  }
})