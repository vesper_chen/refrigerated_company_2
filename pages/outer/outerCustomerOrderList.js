// pages/outer/outerCustomerOrderList.js

const app = getApp()
var webhost = app.globalData.webhost;
import Toast from '../Vant/toast/toast';
import utils from '../../utils/date.js';
var listData;
var instance;

Page({
  data: {
    dataList: [],
    selectedIndex: -1,
    page: 1,
    startDate: '',
    endDate: '',
    inputVal: '',
    inputRouteVal: '',
    sum: 0,
    selectList: [],
    selectedAllStatus: false,
    loading: 0,
    inputShowed: false,
    inputRouteShowed: false
  },

  //发货客户searchBar的js方法
  showInput: function () {
    this.setData({
      inputShowed: true,
      inputRouteShowed: false
    });
  },
  hideInput: function () {
    this.setData({
      inputVal: "",
      inputShowed: false
    });
    listData();
  },
  clearInput: function () {
    this.setData({
      inputVal: ""
    });
  },
  inputTyping: function (e) {
    this.setData({
      inputVal: e.detail.value
    });
  },

  //路线searchBar的js方法
  showRouteInput: function () {
    this.setData({
      inputRouteShowed: true,
      inputShowed: false,
    });
  },
  hideRouteInput: function () {
    this.setData({
      inputRouteVal: "",
      inputRouteShowed: false
    });
    listData();
  },
  clearRouteInput: function () {
    this.setData({
      inputRouteVal: ""
    });
  },
  inputRouteTyping: function (e) {
    this.setData({
      inputRouteVal: e.detail.value
    });
  },


  //输入查询条件后进行搜索
  doSearch: function () {
    var that = this;
    that.setData({
      page: 1,
      loading: 0,
      selectedIndex: -1,
      selectedState: '',
      dataList: []
    })
    listData();
  },


  bindDate: function (e) {
    var that = this;
    var date = e.detail.value;
    that.setData({
      page: 1,
      loading: 0,
      selectedIndex: -1,
      selectedState: '',
      dataList: [],
      selectList:[],
      sum: 0,
      startDate: date
    })
    listData();
  },

  bindDate2: function (e) {
    var that = this;
    var date = e.detail.value;
    that.setData({
      page: 1,
      loading: 0,
      selectedIndex: -1,
      selectedState: '',
      dataList: [],
      selectList: [],
      sum:0,
      endDate: date
    })
    listData();
  },

  bindSelectAll: function () {
    var selectList = this.data.selectList;
    var selectedAllStatus = this.data.selectedAllStatus;
    for (var i = 0; i < selectList.length; i++) {
      selectList[i] = !selectedAllStatus
    }
    this.setData({
      selectList: selectList,
      selectedAllStatus: !selectedAllStatus
    })
    this.sum();
  },

  bindCheckbox: function (e) {
    var index = +e.currentTarget.dataset.index;
    var sum = this.data.sum;
    var selectList = this.data.selectList;
    selectList[index] = !selectList[index];
    var selectedAllStatus = this.data.selectedAllStatus;
    this.setData({
      selectList: selectList
    })
    this.sum();
  },

  sum: function () {
    var sum = 0;
    var selectList = this.data.selectList;
    for (var i = 0; i < selectList.length; i++) {
      if (selectList[i]) {
        sum += 1
      }
    }
    this.setData({
      sum: sum,
      disabled: sum == 0 ? false : true,
    })
    if (sum > 0 && sum == selectList.length) {
      this.setData({
        selectedAllStatus: true
      })
    } else {
      this.setData({
        selectedAllStatus: false
      })
    }
  },

  add: function(){
    var that =this;
    var list = [];
    for (var i = 0; i < that.data.dataList.length; i++){
      if(that.data.selectList[i]){
        list.push(that.data.dataList[i].a1)
      }
    }
    console.log(list)
    var pages = getCurrentPages(); // 获取页面栈
    var currPage = pages[pages.length - 1]; // 当前页面
    var prevPage = pages[pages.length - 2]; // 上一个页面
    prevPage.setData({
      orderIdList: prevPage.data.orderIdList.concat(list)
    })
    prevPage.outerSnapData();
    wx.navigateBack({
      delta: 1
    })
  },


  onLoad: function () {
    console.log(app.globalData)
    var that = this;
    var myDate = new Date();
    var myDate2 = new Date(myDate.getTime() - 30 * 24 * 3600 * 1000);
    var date = utils.Format('yyyy-MM-dd', myDate);
    var date2 = utils.Format('yyyy-MM-dd', myDate2);
    that.setData({
      startDate: date2,
      endDate: date,
      date: date
    })

    listData = function () {
      wx.showNavigationBarLoading();
      var data = [{
        String: {
          sessionid: app.globalData.sessionid,
          a1: that.data.startDate,
          a2: that.data.endDate,
          a3: that.data.inputVal,
          a4: that.data.inputRouteVal,
          a5: '',
          pageNumber: that.data.page,
          pageSize: 5
        }
      }]
      wx.request({
        url: webhost,
        data: {
          eap_username: app.globalData.account,
          eap_password: app.globalData.password,
          boId: "tspdriver_Tsp2BusinessServiceBO_bo", //调用的bo
          methodName: 'queryOuterCustomerOrderList', //调用的具体方法
          returnType: "json", //返回参数类型
          parameters: JSON.stringify(data)
        },
        header: {
          'content-type': 'application/x-www-form-urlencoded'
        },
        method: 'POST',
        complete() {
          wx.hideNavigationBarLoading();
        },
        success({
          data
        }) {
          if (data.code && data.code == '000000') {
            var list = data.dataList;
            var selectList = new Array();
            if (list.length > 0) {
              for(var i=0;i<list.length;i++){
                selectList[i] = false
              }
              that.setData({
                loading: 0,
                dataList: that.data.dataList.concat(list),
                selectList: that.data.selectList.concat(selectList),
                selectedAllStatus: false,
              })
              //当 selectedList中没有true值时，要将‘申请报账’按钮置位不可用
              if (that.data.selectList.indexOf(true) == -1) {
                that.setData({
                  disabled: false
                })
              }
            } else {
              console.log('无数据')
              that.setData({
                loading: 2
              })
            }
          } else if (data.code == '003000') {
            wx.showToast({
              title: data.msg,
              icon: 'none'
            })
            setTimeout(function () {
              wx.redirectTo({
                url: '../login/login',
              })
            }, 1200);
          }
        },
        fail: function (res) {
          wx.showToast({
            title: '网络连接失败',
            icon: 'none'
          })
        }
      })
    }

    listData();


  },

  onPullDownRefresh() {
    var that = this;
    setTimeout(function () {
      that.setData({
        page: 1,
        loading: 0,
        selectedIndex: -1,
        selectedState: '',
        dataList: []
      });
      listData();
      wx.stopPullDownRefresh()
    }, 2000)
  },

  onShareAppMessage() {

  },

  onShow() {
    var that = this;
    // setTimeout(function () {
    //   that.setData({
    //     page: 1,
    //     dataList: []
    //   })
    //   if (that.data.active == 0) {
    //     listData(that.data.startDate, that.data.endDate, '');
    //   } else {
    //     listData(that.data.startDate, that.data.endDate, '')
    //   }
    // }, 500)
  },

  onReachBottom() {
    var that = this;
    console.log(that.data.loading);
    if (that.data.loading == 0) {
      that.setData({
        loading: true,
        page: that.data.page + 1
      })
      setTimeout(() => {
        if (that.data.active == 0) {
          listData('PLAN0010');
        } else {
          listData('PLAN3040', that.data.startDate, that.data.endDate)
        }
      }, 300);
    }
  }
})