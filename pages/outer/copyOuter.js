// pages/outer/copyOuter.js
var app = getApp();
var webhost = app.globalData.webhost;
var upLoadHost = app.globalData.upLoadHost;
var util = require('../../utils/util.js');
var utilDate = require('../../utils/date.js');
var customerData, routeData, carData, driverData, unitData, standardList, outerSnapData, copyOuterData, getPermissionData;

Page({
  data: {
    createDate: '',
    outerSnapList: [],
    orderIdList: [],
    selectDeliveryList: [],
    route: { a1: '', a2: '', a3: '' },
    car: { a1: '', a2: '', a3: '' },
    driver: { a1: '', a2: '', a3: '' },
    customer: { a1: '', a2: '', a3: '' },
    unit: {a1:'', a2: ''},
    boxList: [],
    loading: 0,
    boxPage: 1,
    pageSize: 10,

    carryDate: '',
    standardIndex: 0,
    fee: {},
    estimatedProfit: '',

    // addDeliveryItem: {},
    isShow: false,
    inputShowed: true,
    disabled: false,
    imgs: [],
    upImgs: [],
  },

  //根据选择的发货客户获取发货点信息
  outerSnapData: function () {
    var that = this;
    wx.showNavigationBarLoading();
    var data = [{
      String: {
        sessionid: app.globalData.sessionid,
        data: that.data.orderIdList,
      }
    }]
    wx.request({
      url: webhost,
      data: {
        eap_username: app.globalData.account,
        eap_password: app.globalData.password,
        boId: "tspdriver_Tsp2BusinessServiceBO_bo", //调用的bo
        methodName: 'getOuterCarPayFeeSnapByOrderIds', //调用的具体方法
        returnType: "json", //返回参数类型
        parameters: JSON.stringify(data)
      },
      header: {
        'content-type': 'application/x-www-form-urlencoded'
      },
      method: 'POST',
      complete() {
        wx.hideNavigationBarLoading();
      },
      success({ data }) {
        if (data.code && data.code == '000000') {
          var list = data.dataList;
          var selectList = [];
          if (list.length > 0) {
            for (var i = 0; i < list.length; i++) {
              selectList[i] = false
            }
            that.setData({
              outerSnapList: list,
              selectDeliveryList: selectList
            })
          }
        } else if (data.code == '003000') {
          wx.showToast({
            title: data.msg,
            icon: 'none'
          })
          setTimeout(function () {
            wx.redirectTo({
              url: '../login/login',
            })
          }, 1200);
        }
      },
      fail: function (res) {
        wx.showToast({
          title: '网络连接失败',
          icon: 'none'
        })
      }
    })
  },


  // 浮窗选择框的js方法
  //显示浮窗
  showSearchBox: function (e) {
    var that = this;
    var sign = e.currentTarget.dataset.sign;
    that.setData({
      inputVal: '',
      selectedIndex: -1,
      boxList: [],
      boxPage: 1,
      loading: 0,
      sign: sign
    })
    switch (sign) {
      case 'route':
        that.setData({
          placeHolder: '路线名称',
        })
        routeData();
        break;
      case 'customer':
        that.setData({
          placeHolder: '客户名称',
        })
        customerData();
        break;
      case 'car':
        that.setData({
          placeHolder: '车号',
        })
        carData();
        break;
      case 'driver':
        that.setData({
          placeHolder: '司机',
        })
        driverData();
        break;
      case 'unit':
        that.setData({
          placeHolder: '责任小组',
        })
        unitData();
        break; 
    }
    that.setData({
      showBox: true,
    })
    // customerData();
  },
  //显示输入框
  showInput: function () {
    this.setData({
      inputShowed: true
    });
  },
  //隐藏输入框
  hideInput: function () {
    this.setData({
      inputVal: "",
      selectedIndex: -1,
      boxPage: 1,
      loading: 1,
      boxList: [],
    });
    switch (this.data.sign) {
      case 'route':
        routeData();
        break;
      case 'customer':
        customerData();
        break;
      case 'car':
        carData();
        break;
      case 'driver':
        driverData();
        break;
      case 'unit':
        unitData();
        break;
    }
  },
  //清楚输入框内容
  clearInput: function () {
    this.setData({
      inputVal: ""
    });
  },
  //输入框中输入内容时记录输入值
  inputTyping: function (e) {
    clearTimeout(this.data.t)
    this.setData({
      inputVal: e.detail.value,
      boxList: [],
      loading: 1,
      boxPage: 1,
      selectedIndex: -1,
    });
    switch (this.data.sign) {
      case 'route':
        this.setData({
          t: setTimeout(function () {
            routeData();
          }, 500)
        })
        break;
      case 'customer':
        this.setData({
          t: setTimeout(function () {
            customerData();
          }, 500)
        })
        break;
      case 'car':
        this.setData({
          t: setTimeout(function () {
            carData();
          }, 500)
        })
        break;
      case 'driver':
        this.setData({
          t: setTimeout(function () {
            driverData();
          }, 500)
        })
        break;
      case 'unit':
        this.setData({
          t: setTimeout(function () {
            unitData();
          }, 500)
        })
        break;
    }

  },
  //使用输入框中的值进行搜索
  doSearch: function () {
    var that = this;
    that.setData({
      boxPage: 1,
      boxList: [],
      selectedIndex: -1,
    })
    switch (that.data.sign) {
      case 'route':
        routeData();
        break;
      case 'customer':
        customerData();
        break;
      case 'car':
        carData();
        break;
      case 'driver':
        driverData();
        break;
      case 'unit':
        unitData();
        break;
    }
  },
  //显示弹窗时禁止页面滚动
  stopPageScroll: function () {
    return
  },
  //关闭浮窗
  cancel: function () {
    this.setData({
      inputVal: '',
      showBox: false,
      selectedIndex: -1,
    })
  },
  //在浮窗的加载数据中选择需要的一条数据
  select: function (e) {
    var that = this;
    var index = +e.currentTarget.dataset.id;
    that.setData({
      selectedIndex: index
    })
  },
  //对浮窗内的数据列表进行滚动加载更多数据
  more: function () {
    var that = this;
    if (that.data.loading == 0) {
      that.setData({
        loading: true,
        boxPage: that.data.boxPage + 1
      })
      setTimeout(() => {
        switch (that.data.sign) {
          case 'route':
            routeData();
            break;
          case 'customer':
            customerData();
            break;
          case 'car':
            carData();
            break;
          case 'driver':
            driverData();
            break;
          case 'unit':
            unitData();
            break;
        }
      }, 300);
    }
  },
  //选择一条数据后，确认选择结果
  submit: function () {
    var that = this;
    var index = that.data.selectedIndex;
    var list = that.data.boxList;

    switch (that.data.sign) {
      case 'route':
        that.setData({
          route: list[index]
        })
        break;
      case 'customer':
        that.setData({
          customer: list[index]
        })
        break;
      case 'car':
        that.setData({
          car: list[index]
        })
        break;
      case 'driver':
        that.setData({
          driver: list[index]
        })
        break;
      case 'unit':
        that.setData({
          unit: list[index]
        })
        break;
    }
    setTimeout(function () {
      that.setData({
        showBox: false,
        inputVal: '',
        selectedIndex: -1,
      });
    }, 50)
  },

  //修改承运日期
  changeCarryDate: function (e) {
    console.log(e)
    this.setData({
      carryDate: e.detail.value
    })
  },

  //修改是否达标
  changeStandard: function (e) {
    this.setData({
      standardIndex: +e.detail.value
    })
  },

  //手动输入结算明细金额
  inputSettle: function (e) {
    var kind = e.currentTarget.dataset.kind;
    if (kind == 'a2') {
      var item = 'fee.a2';
      this.setData({
        [item]: e.detail.value
      })
    } else if (kind == 'a3') {
      var item = 'fee.a3';
      this.setData({
        [item]: e.detail.value
      })
    }
    //计算运费总额
    var sum = 'fee.a1';
    if (this.data.fee.a2 && this.data.fee.a3) {
      this.setData({
        [sum]: (+this.data.fee.a2 + +this.data.fee.a3).toFixed(2)
      })
    } else {
      this.setData({
        [sum]: ''
      })
    }

    //计算预估利润
    var fee = this.data.fee;
    if (fee.a2 && fee.a3 && fee.a5 && fee.a6 && fee.a7) {
      var estimatedProfit = (+fee.a1 - +fee.a4 - +fee.a2 / 1.1 * 0.1 - +fee.a2 / 1.1 * 0.1 * 0.1 + +fee.a5 / 1.16 * 0.16 - +fee.a2 / 1.1 * 0.04 * 0.25 + +fee.a7 / 1.1 * 0.1) * 100 / +fee.a1
      this.setData({
        estimatedProfit: (estimatedProfit / 100).toFixed(4),
        estimatedProfitPercent: estimatedProfit.toFixed(2) + '%'
      })
    }
  },

  //手动输入支付明细金额
  inputPay: function (e) {
    var kind = e.currentTarget.dataset.kind;
    if (kind == 'a5') {
      var item = 'fee.a5';
      this.setData({
        [item]: e.detail.value
      })
    } else if (kind == 'a6') {
      var item = 'fee.a6';
      this.setData({
        [item]: e.detail.value
      })
    } else if (kind == 'a7') {
      var item = 'fee.a7';
      this.setData({
        [item]: e.detail.value
      })
    }

    var sum = 'fee.a4';
    if (this.data.fee.a5 && this.data.fee.a6 && this.data.fee.a7) {
      this.setData({
        [sum]: (+this.data.fee.a5 + +this.data.fee.a6 + +this.data.fee.a7).toFixed(2)
      })
    } else {
      this.setData({
        [sum]: ''
      })
    }

    //计算预估利润
    var fee = this.data.fee;
    if (fee.a2 && fee.a3 && fee.a5 && fee.a6 && fee.a7) {
      var estimatedProfit = (+fee.a1 - +fee.a4 - +fee.a2 / 1.1 * 0.1 - +fee.a2 / 1.1 * 0.1 * 0.1 + +fee.a5 / 1.16 * 0.16 - +fee.a2 / 1.1 * 0.04 * 0.25 + +fee.a7 / 1.1 * 0.1) * 100 / +fee.a1
      this.setData({
        estimatedProfit: (estimatedProfit / 100).toFixed(4),
        estimatedProfitPercent: estimatedProfit.toFixed(2) + '%'
      })
    }
  },


  //新建外调申请单时，为新单增添客户订单明细
  toOrderDetail: function () {
    // var id = this.data.deliverCustomerId;
    // var code = this.data.deliverCustomerCode;
    // var name = this.data.deliverCustomerName;
    wx.navigateTo({
      url: './outerCustomerOrderList'
    })
  },

  //使用新增发货点信息页面传回来的item动态添加到deliveryInfo中
  addDelivery: function () {
    var that = this;
    var curDate = utilDate.getFormatDateYMDHM();
    var timeList = that.data.deliveryTimeList;
    var info = that.data.deliveryInfo;
    var item = that.data.addDeliveryItem;
    timeList.push(curDate);
    info.push(item);
    that.setData({
      deliveryInfo: info,
      deliveryTimeList: timeList
    })

    //直接使用下方的方式修改data中的deliveryInfo可修改成功，但是不会重新渲染页面。。。
    // that.data.deliveryInfo.push(that.data.addDeliveryItem)
  },


  //勾选发货点信息列表
  bindCheckbox: function (e) {
    var index = +e.currentTarget.dataset.index;
    var selectDeliveryList = this.data.selectDeliveryList;
    selectDeliveryList[index] = !selectDeliveryList[index];
    this.setData({
      selectDeliveryList: selectDeliveryList
    })
  },

  ifNullShowToast: function (value, text) {
    if (!value) {
      wx.showToast({
        title: text,
        icon: 'none'
      })
      return false;
    } else {
      return true;
    }
  },

  //提交新建报单操作
  saveOuterCarApply: function () {
    var that = this;
    wx.showNavigationBarLoading();
    var fee = that.data.fee;
    var pictures = that.data.imgs;
    var list = [];
    if (that.data.outerSnapList.length > 0) {
      for (var i = 0; i < that.data.outerSnapList.length; i++) {
        if (that.data.selectDeliveryList[i]) {
          var temp = { b1: that.data.outerSnapList[i].a1, }
          list.push(temp);
        }
      }
      console.log(list)
    }
    if (that.ifNullShowToast(list.length, '请至少选择一条客户订单明细')) {
      if (that.ifNullShowToast(that.data.route.a1, '请选择路线')) {
        if (that.ifNullShowToast(that.data.car.a1, '请选择车号')) {
          if (that.ifNullShowToast(that.data.driver.a1, '请选择司机')) {
            if (that.ifNullShowToast(that.data.customer.a1, '请选择结算对象')) {
              if (that.ifNullShowToast(that.data.unit.a2, '请选择责任小组')){
                if (that.ifNullShowToast(fee.a2, '请输入账期运费')) {
                  if (that.ifNullShowToast(fee.a3, '请输入现金运费')) {
                    if (that.ifNullShowToast(fee.a5, '请输入油卡支付')) {
                      if (that.ifNullShowToast(fee.a6, '请输入现金支付')) {
                        if (that.ifNullShowToast(fee.a7, '请输入开票金额')) {
                          console.log('输入完整')
                          wx.showNavigationBarLoading();
                          var data = [{
                            String: {
                              sessionid: app.globalData.sessionid,
                              a1: that.data.route.a1,
                              a2: that.data.route.a3,
                              a3: that.data.carryDate,
                              a4: that.data.car.a1,
                              a5: that.data.car.a3,
                              a6: that.data.driver.a1,
                              a7: that.data.driver.a3,
                              a8: that.data.customer.a1,
                              a9: that.data.customer.a3,
                              a10: that.data.standardValueList[that.data.standardIndex],
                              a11: that.data.estimatedProfit,
                              a12: that.data.unit.a1,
                              a13: that.data.unit.a2,
                              fee: fee,//结算明细和支付明细
                              list: list,
                              pictures: pictures,//附件图片地址
                            }
                          }]
                          wx.request({
                            url: webhost,
                            data: {
                              eap_username: app.globalData.account,
                              eap_password: app.globalData.password,
                              boId: "tspdriver_Tsp2BusinessServiceBO_bo", //调用的bo
                              methodName: 'saveOuterCarApply', //调用的具体方法
                              returnType: "json", //返回参数类型
                              parameters: JSON.stringify(data)
                            },
                            header: {
                              'content-type': 'application/x-www-form-urlencoded'
                            },
                            method: 'POST',
                            complete() {
                              wx.hideNavigationBarLoading();
                            },
                            success({ data }) {
                              if (data.code && data.code == '000000') {
                                wx.showToast({
                                  title: '新建成功',
                                })
                                setTimeout(function () {
                                  wx.navigateBack({
                                    delta: 1
                                  })
                                }, 1500)
                              } else if (data.code == '003000') {
                                wx.showToast({
                                  title: data.msg,
                                  icon: 'none'
                                })
                                setTimeout(function () {
                                  wx.redirectTo({
                                    url: '../login/login',
                                  })
                                }, 1200);
                              } else {
                                wx.showToast({
                                  title: data.msg,
                                  icon: 'none'
                                })
                              }
                            },
                            fail: function (res) {
                              wx.showToast({
                                title: '网络连接失败',
                                icon: 'none'
                              })
                            }
                          })
                        }
                      }
                    }
                  }
                }
              }
            }
          }
        }
      }
    }
    
  },

  goBackList: function () {
    wx.navigateBack({
      delta: 1
    })
  },

  // 上传图片
  upLoadImg(tempFilePaths, i) {
    var that = this;
    var upPicData = that.data.upPicData;
    wx.showLoading({
      title: '图片上传中',
    })
    var fileName = upPicData.dir + '/' + app.globalData.account + utilDate.Format("yyyyMMddhhmmssS", new Date());
    var url = upLoadHost;
    var imgs = that.data.imgs;
    var item = {
      picturePath: url + '/' + fileName
    }
    const uploadTask = wx.uploadFile({
      url: url,
      filePath: tempFilePaths[i],
      name: 'file',
      formData: {
        name: tempFilePaths[i],
        key: fileName,
        policy: upPicData.policy,
        OSSAccessKeyId: upPicData.accessid,
        success_action_status: "200",
        signature: upPicData.signature
      },
      success(res) {
        imgs.push(item);
        that.setData({
          imgs: imgs
        })
        i++;
        if (i < tempFilePaths.length) {
          that.upLoadImg(tempFilePaths, i);
        } else {
          wx.hideLoading();
        }
      },
      fail() {
        wx.showToast({
          title: '第' + i + '张图片上传失败',
          icon: 'none'
        })
      }
    })
    uploadTask.onProgressUpdate((res) => {
      console.log('上传进度', res.progress)
      console.log('已经上传的数据长度', res.totalBytesSent)
      console.log('预期需要上传的数据总长度', res.totalBytesExpectedToSend)
    })
  },

  picClick() {
    var that = this;
    if (!that.data.upPicData.accessid) {
      wx.showToast({
        title: '未获取上传权限',
        icon: 'none'
      })
      return false;
    }
    var count = 9;
    count = 9 - that.data.imgs.length;
    if (count <= 0) {
      wx.showToast({
        title: '最多上传9张图片',
        icon: 'none'
      })
      return false;
    }
    wx.chooseImage({
      count: count,
      sizeType: ['original', 'compressed'],
      sourceType: ['album', 'camera'],
      success(res) {
        var tempFilePaths = res.tempFilePaths;
        if (tempFilePaths) {
          var i = 0;
          that.upLoadImg(tempFilePaths, i);
        }
      }
    })
  },


  // 删除图片
  deleteImg: function (e) {
    var imgs = this.data.imgs;
    var index = e.currentTarget.dataset.index;
    imgs.splice(index, 1);
    this.setData({
      imgs: imgs
    });
  },
  // 预览图片
  previewImg: function (e) {
    //获取当前图片的下标
    var index = e.currentTarget.dataset.index;
    //所有图片
    var imgs = this.data.imgs;
    var preImgs = [];
    for (var i = 0; i < imgs.length; i++) {
      preImgs[i] = imgs[i].picturePath;
    }
    wx.previewImage({
      //当前显示图片
      current: preImgs[index],
      //所有图片
      urls: preImgs
    })
  },



  onLoad: function (options) {
    var that = this;
    var curDate = utilDate.getFormatDate();
    var outerId = options.outerId;
    that.setData({
      createDate: curDate,
      // carryDate: curDate
    })

    //复制驳回外调单时获取原外调单所有数据
    copyOuterData = function () {
      wx.showNavigationBarLoading();
      var data = [{
        String: {
          sessionid: app.globalData.sessionid,
          a1: outerId,
        }
      }]
      wx.request({
        url: webhost,
        data: {
          eap_username: app.globalData.account,
          eap_password: app.globalData.password,
          boId: "tspdriver_Tsp2BusinessServiceBO_bo", //调用的bo
          methodName: 'copyOutCarAppy', //调用的具体方法
          returnType: "json", //返回参数类型
          parameters: JSON.stringify(data)
        },
        header: {
          'content-type': 'application/x-www-form-urlencoded'
        },
        method: 'POST',
        complete() {
          wx.hideNavigationBarLoading();
        },
        success({ data }) {
          if (data.code && data.code == '000000') {
            var data = data.data;
            that.setData({
              ['route.a1']: data.a1,
              ['route.a3']: data.a2,
              carryDate: data.a3,
              ['customer.a1']: data.a8,
              ['customer.a3']: data.a9,
              ['car.a1']: data.a4,
              ['car.a3']: data.a5,
              ['driver.a1']: data.a6,
              ['driver.a3']: data.a7,
              ['unit.a1']: data.a12,
              ['unit.a2']: data.a13,
              standardIndex: parseInt(data.a10),
              estimatedProfit: data.a11,
              estimatedProfitPercent: (data.a11 * 100).toFixed(2) + '%',
              fee: data.fee,
              outerSnapList: data.list,
              imgs: data.pictures
            })
            var orderIdList = [];
            for(var i=0; i<data.list.length; i++){
              orderIdList.push(data.list[i].a1)
            }
            that.setData({
              orderIdList: orderIdList
            })
          } else if (data.code == '003000') {
            wx.showToast({
              title: data.msg,
              icon: 'none'
            })
            setTimeout(function () {
              wx.redirectTo({
                url: '../login/login',
              })
            }, 1200);
          }
        },
        fail: function (res) {
          wx.showToast({
            title: '网络连接失败',
            icon: 'none'
          })
        }
      })
    };

    //获取结算客户列表
    customerData = function () {
      wx.showNavigationBarLoading();
      var data = [{
        String: {
          sessionid: app.globalData.sessionid,
          a1: that.data.inputVal,
          pageNumber: that.data.boxPage,
          pageSize: 10
        }
      }]
      wx.request({
        url: webhost,
        data: {
          eap_username: app.globalData.account,
          eap_password: app.globalData.password,
          boId: "tspdriver_Tsp2BusinessServiceBO_bo", //调用的bo
          methodName: 'queryDeliverGoodCustomerList', //调用的具体方法
          returnType: "json", //返回参数类型
          parameters: JSON.stringify(data)
        },
        header: {
          'content-type': 'application/x-www-form-urlencoded'
        },
        method: 'POST',
        complete() {
          wx.hideNavigationBarLoading();
        },
        success({ data }) {
          if (data.code && data.code == '000000') {
            var list = data.dataList;
            if (list.length > 0) {
              that.setData({
                loading: 0,
                boxList: that.data.boxList.concat(list)
              })
            } else {
              that.setData({
                loading: 2
              })
            }
          } else if (data.code == '003000') {
            wx.showToast({
              title: data.msg,
              icon: 'none'
            })
            setTimeout(function () {
              wx.redirectTo({
                url: '../login/login',
              })
            }, 1200);
          }
        },
        fail: function (res) {
          wx.showToast({
            title: '网络连接失败',
            icon: 'none'
          })
        }
      })
    };
    //获取路线列表
    routeData = function () {
      wx.showNavigationBarLoading();
      var data = [{
        String: {
          sessionid: app.globalData.sessionid,
          a1: that.data.inputVal,
          pageNumber: that.data.boxPage,
          pageSize: 10
        }
      }]
      wx.request({
        url: webhost,
        data: {
          eap_username: app.globalData.account,
          eap_password: app.globalData.password,
          boId: "tspdriver_Tsp2BusinessServiceBO_bo", //调用的bo
          methodName: 'queryRoutesList', //调用的具体方法
          returnType: "json", //返回参数类型
          parameters: JSON.stringify(data)
        },
        header: {
          'content-type': 'application/x-www-form-urlencoded'
        },
        method: 'POST',
        complete() {
          wx.hideNavigationBarLoading();
        },
        success({ data }) {
          if (data.code && data.code == '000000') {
            var list = data.dataList;
            if (list.length > 0) {
              that.setData({
                loading: 0,
                boxList: that.data.boxList.concat(list)
              })
            } else {
              that.setData({
                loading: 2
              })
            }
          } else if (data.code == '003000') {
            wx.showToast({
              title: data.msg,
              icon: 'none'
            })
            setTimeout(function () {
              wx.redirectTo({
                url: '../login/login',
              })
            }, 1200);
          }
        },
        fail: function (res) {
          wx.showToast({
            title: '网络连接失败',
            icon: 'none'
          })
        }
      })
    };
    //获取客车号列表
    carData = function () {
      wx.showNavigationBarLoading();
      var data = [{
        String: {
          sessionid: app.globalData.sessionid,
          a1: that.data.inputVal,
          pageNumber: that.data.boxPage,
          pageSize: 10
        }
      }]
      wx.request({
        url: webhost,
        data: {
          eap_username: app.globalData.account,
          eap_password: app.globalData.password,
          boId: "tspdriver_Tsp2BusinessServiceBO_bo", //调用的bo
          methodName: 'queryCarList', //调用的具体方法
          returnType: "json", //返回参数类型
          parameters: JSON.stringify(data)
        },
        header: {
          'content-type': 'application/x-www-form-urlencoded'
        },
        method: 'POST',
        complete() {
          wx.hideNavigationBarLoading();
        },
        success({ data }) {
          if (data.code && data.code == '000000') {
            var list = data.dataList;
            if (list.length > 0) {
              that.setData({
                loading: 0,
                boxList: that.data.boxList.concat(list)
              })
            } else {
              that.setData({
                loading: 2
              })
            }
          } else if (data.code == '003000') {
            wx.showToast({
              title: data.msg,
              icon: 'none'
            })
            setTimeout(function () {
              wx.redirectTo({
                url: '../login/login',
              })
            }, 1200);
          }
        },
        fail: function (res) {
          wx.showToast({
            title: '网络连接失败',
            icon: 'none'
          })
        }
      })
    };

    //获取司机列表
    driverData = function () {
      wx.showNavigationBarLoading();
      var data = [{
        String: {
          sessionid: app.globalData.sessionid,
          a1: that.data.inputVal,
          pageNumber: that.data.boxPage,
          pageSize: 10
        }
      }]
      wx.request({
        url: webhost,
        data: {
          eap_username: app.globalData.account,
          eap_password: app.globalData.password,
          boId: "tspdriver_Tsp2BusinessServiceBO_bo", //调用的bo
          methodName: 'queryDriverList', //调用的具体方法
          returnType: "json", //返回参数类型
          parameters: JSON.stringify(data)
        },
        header: {
          'content-type': 'application/x-www-form-urlencoded'
        },
        method: 'POST',
        complete() {
          wx.hideNavigationBarLoading();
        },
        success({ data }) {
          if (data.code && data.code == '000000') {
            var list = data.dataList;
            if (list.length > 0) {
              that.setData({
                loading: 0,
                boxList: that.data.boxList.concat(list)
              })
            } else {
              that.setData({
                loading: 2
              })
            }
          } else if (data.code == '003000') {
            wx.showToast({
              title: data.msg,
              icon: 'none'
            })
            setTimeout(function () {
              wx.redirectTo({
                url: '../login/login',
              })
            }, 1200);
          }
        },
        fail: function (res) {
          wx.showToast({
            title: '网络连接失败',
            icon: 'none'
          })
        }
      })
    };

    //获取责任小组列表
    unitData = function (init) {
      wx.showNavigationBarLoading();
      var data = [{
        String: {
          sessionid: app.globalData.sessionid,
          a1: that.data.inputVal,
          pageNumber: that.data.boxPage,
          pageSize: 10
        }
      }]
      wx.request({
        url: webhost,
        data: {
          eap_username: app.globalData.account,
          eap_password: app.globalData.password,
          boId: "tspdriver_Tsp2BusinessServiceBO_bo", //调用的bo
          methodName: 'getUnit', //调用的具体方法
          returnType: "json", //返回参数类型
          parameters: JSON.stringify(data)
        },
        header: {
          'content-type': 'application/x-www-form-urlencoded'
        },
        method: 'POST',
        complete() {
          wx.hideNavigationBarLoading();
        },
        success({ data }) {
          if (data.code && data.code == '000000') {
            var list = data.list;
            if (list.length > 0) {
              if (init == 0) {
                that.setData({
                  loading: 0,
                  unit: list[0]
                })
              } else {
                that.setData({
                  loading: 0,
                  boxList: that.data.boxList.concat(list)
                })
              }
            } else {
              that.setData({
                loading: 2
              })
            }
          } else if (data.code == '003000') {
            wx.showToast({
              title: data.msg,
              icon: 'none'
            })
            setTimeout(function () {
              wx.redirectTo({
                url: '../login/login',
              })
            }, 1200);
          } else {
            wx.showToast({
              title: data.msg,
              icon: 'none'
            })
          }
        },
        fail: function (res) {
          wx.showToast({
            title: '网络连接失败',
            icon: 'none'
          })
        }
      })
    };

    //获取是否达标的码表
    standardList = function () {
      wx.showNavigationBarLoading();
      var data = [{
        String: {
          codeListKey: 'comm_isYesOrNo'
        }
      }]
      wx.request({
        url: webhost,
        data: {
          eap_username: app.globalData.account,
          eap_password: app.globalData.password,
          boId: "tspdriver_Tsp2BusinessServiceBO_bo", //调用的bo
          methodName: 'getCodeList', //调用的具体方法
          returnType: "json", //返回参数类型
          parameters: JSON.stringify(data)
        },
        header: {
          'content-type': 'application/x-www-form-urlencoded'
        },
        method: 'POST',
        complete() {
          wx.hideNavigationBarLoading();
        },
        success({ data }) {
          if (data.code && data.code == '000000') {
            var list = data.codeList;
            var code = [];
            var name = [];
            if (list.length > 0) {
              for (var i = 0; i < list.length; i++) {
                code[i] = list[i].codeValue;
                name[i] = list[i].codeName;
              }
            }
            that.setData({
              standardValueList: code,
              standardNameList: name,
            })
          } else if (data.code == '003000') {
            wx.showToast({
              title: data.msg,
              icon: 'none'
            })
            setTimeout(function () {
              wx.redirectTo({
                url: '../login/login',
              })
            }, 1200);
          }
        },
        fail: function (res) {
          wx.showToast({
            title: '网络连接失败',
            icon: 'none'
          })
        }
      })
    };

    //获取上传权限
    getPermissionData = function(){
      var permissionParam = [{
        String: {
          sessionid: app.globalData.sessionid
        }
      }]
      wx.request({
        url: webhost,
        data: {
          eap_username: app.globalData.account,
          eap_password: app.globalData.password,
          boId: "tspdriver_Tsp2BusinessServiceBO_bo", //调用的bo
          methodName: 'getFileUploadPermission', //调用的具体方法
          returnType: "json", //返回参数类型
          parameters: JSON.stringify(permissionParam)
        },
        header: {
          'content-type': 'application/x-www-form-urlencoded'
        },
        method: 'POST',
        success: function (res) {
          var code = res.data.code;
          if (code == '000000') {
            var data = res.data.data;
            that.setData({
              upPicData: data,
            })
          } else {
            wx.showModal({
              title: '提示',
              showCancel: false,
              confirmColor: '#118EDE',
              content: res.data.msg,
              success: function (res) {
                if (code == '003000') {
                  if (res.confirm) {
                    wx.reLaunch({
                      url: '../login/login'
                    })
                  }
                }
              }
            })
          }
        }
      })
    }
    getPermissionData();

    setTimeout(function(){
      standardList();
    },500)

    setTimeout(function () {
      copyOuterData();
    }, 1500)

    

    
  },

  onShow: function (options) {
  },

  onPullDownRefresh: function () {
    setTimeout(() => {
      wx.stopPullDownRefresh();
    }, 1500);
  },

  onReachBottom: function () {
    // var that = this;
    // if (that.data.loading == 0) {
    //   that.setData({
    //     loading: true,
    //     pageNumber: that.data.pageNumber + 1
    //   })
    //   setTimeout(() => {
    //     that.queryPlanForApplyReim();
    //   }, 300);
    // }
  },


})