var app = getApp();
var util = require('../../utils/md5.js');
var webhost = app.globalData.webhost;
import Notify from '../Vant/notify/notify';
var Login;

Page({
  data: { 
    account: '',
    password: ''
  },

  bindAccount(e){
    this.setData({
      account: e.detail.value
    })
  },

  bindPwd(e) {
    this.setData({
      password: e.detail.value
    })
  },

  Login(){
    var that = this;
    Login();
  },

  onLoad: function (options) {
    var that = this;

    Login = function(){
      var account = that.data.account;
      var password = that.data.password;
      if (account === "" || password === '') {
        Notify({
          text: '账号或密码不能为空！',
          duration: 1500
        }) 
        return false;
      }
      wx.showNavigationBarLoading();
      // password = util.hexMD5(password); 
      var data = [{
        String: {
        }
      }] 
      wx.request({
        url: webhost,
        data: {
          eap_username: account,
          eap_password: password,
          boId: "tspdriver_Tsp2BusinessServiceBO_bo", //调用的bo
          methodName: 'login', //调用的具体方法
          returnType: "json", //返回参数类型
          parameters: JSON.stringify(data)
        },
        method: 'POST',
        header: {
          'content-type': 'application/x-www-form-urlencoded' //默认值
        },
        dataType: 'json',
        complete(){
          wx.hideNavigationBarLoading();       
        },
        success: function (res) {  
          if (res.statusCode == '403'){
            wx.showToast({
              title: '账号或密码错误',
            })
          }
          var data = res.data;
          if (data.code && data.code == '000000') {
            console.log(data.data)
            wx.setStorage({
              key: "xylc_session",
              data: {
                session: data.data
              }
            })
            wx.setStorage({
              key: "xylc_password",
              data: {
                password: password
              }
            })
            app.globalData.user = data.data;
            app.globalData.sessionid = data.data.sessionid;
            app.globalData.name = data.data.name;
            app.globalData.account = data.data.account;
            app.globalData.unitName = data.data.unitName;
            app.globalData.roleList = data.data.roleList;
            app.globalData.password = password;
            console.log(app.globalData)
            wx.showToast({
              title: '登录成功！',
            });
            setTimeout(function(){
              wx.switchTab({
                url: '../business/index',
              })
            },1200);
          } else {
            wx.showToast({
              title: data.msg,
              icon: 'none'
            })
          }
        },
        fail: function (res) {
          wx.showToast({
            title: '网络连接失败',
            icon: 'loading'
          })
        }
      })
    }

  },

  onReady: function () {

  },

  onShow: function () {

  },

  onShareAppMessage: function () {

  }
})