const app = getApp()
var webhost = app.globalData.webhost;
var getCodeList;

Page({
  data: {
    list: [{ nav_url: '../declaration/list', img_url: '../images/business_bg.png', name: '业务报单' }, { nav_url: '../outer/outerCarApplyList', img_url: '../images/outer_bg.png', name: '外调申请' }, { nav_url: '../approval/list', img_url: '../images/approval_bg.png', name: '审批' }]
  },

  openWin(e) {
    var url = e.currentTarget.dataset.url;
    wx.navigateTo({
      url: url
    })
  },

  onShow(){
    getCodeList = function () {
      wx.showNavigationBarLoading();
      var data = [{
        String: {
          codeListKey: 'global_status',
        }
      }]
      wx.request({
        url: webhost,
        data: {
          eap_username: app.globalData.account,
          eap_password: app.globalData.password,
          boId: "tspdriver_Tsp2BusinessServiceBO_bo", //调用的bo
          methodName: 'getCodeList', //调用的具体方法
          returnType: "json", //返回参数类型
          parameters: JSON.stringify(data)
        },
        header: {
          'content-type': 'application/x-www-form-urlencoded'
        },
        method: 'POST',
        success: function (res) {
          console.log('接口post成功')
          if (res.statusCode == '403') {
            wx.showToast({
              title: '登录状态已失效',
              icon:'none'
            });
            setTimeout(function () {
              wx.redirectTo({
                url: '../login/login',
              })
            }, 1500);
          }
        },
        fail: function (res) {
          wx.showModal({
            title: '提示',
            showCancel: false,
            confirmColor: '#118EDE',
            content: '服务器异常',
            success: function (res) {
              if (res.confirm) { }
            }
          })
        }
      })
    }

    setTimeout(function(){
      getCodeList()
    },2000)
  }
  
})