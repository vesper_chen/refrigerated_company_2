var app = getApp();

Page({
  data: {

  },

  openPersonWin(){
    wx.navigateTo({
      url: './personal',
    })
  },

  setClick(){
    wx.navigateTo({
      url: './setPassword',
    })
  },

  logoutClick(){
    wx.showActionSheet({
      itemList: ['退出登录'],
      success(res) {
        if (res.tapIndex == 0){
          wx.showToast({ title: '退出登录成功！' }) 
          app.globalData.user = {};
          app.globalData.sessionid = '';
          app.globalData.name = '';
          app.globalData.roleList = '';
          app.globalData.account = '';
          app.globalData.password = '';
          app.globalData.driverCode = '';
          wx.removeStorage({
            key: 'xylc_session'
          })
          setTimeout(() => {
            wx.reLaunch({
              url: '../login/login'
            })
          }, 1200)
        }
      }
    })
  },

  onLoad: function (options) {
    console.log(app.globalData.name)
    var that = this;

    that.setData({
      name: app.globalData.name
    })
  },
  
  onShow: function () {

  },

  onHide: function () {

  },

  onPullDownRefresh: function () {

  },

  onReachBottom: function () {

  },

  onShareAppMessage: function () {

  }
})