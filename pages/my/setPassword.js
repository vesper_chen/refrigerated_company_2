var app = getApp();
var util = require('../../utils/md5.js');
var webhost = app.globalData.webhost;
import Notify from '../Vant/notify/notify';
var Set;

Page({
  data: {
    oldpassword: '',
    password: '',
    rpassword: ''
  },


  bindPwd1(e) {
    this.setData({
      oldpassword: e.detail.value
    })
  },

  bindPwd2(e) {
    this.setData({
      password: e.detail.value
    })
  },

  bindPwd3(e) {
    this.setData({
      rpassword: e.detail.value
    })
  },

  Login() {
    var that = this;
    Set();
  },

  onLoad: function (options) {
    var that = this;

    Set = function () {
      var oldpassword = that.data.oldpassword;
      var password = that.data.password;
      var rpassword = that.data.rpassword;
      if (oldpassword === "" || password === '' || rpassword === '') {
        Notify({
          text: '密码不能为空！',
          duration: 1500
        })
        return false;
      }
      if ( password != rpassword ) {
        Notify({
          text: '两次密码不一致！',
          duration: 1500
        })
        return false;
      }
      wx.showNavigationBarLoading();
      oldpassword = util.hexMD5(oldpassword);
      password = util.hexMD5(password);
      var data = [{
        String: {
          driverAccount: app.globalData.driverAccount,
          password: oldpassword,
          newPassword: password
        }
      }]
      wx.request({
        url: webhost,
        data: {
          eap_username: "driver",
          eap_password: "Driver2018",
          boId: "tspdriver_tsp2DriverServiceBO_bo", //调用的bo
          methodName: 'modifyPassword', //调用的具体方法
          returnType: "json", //返回参数类型
          parameters: JSON.stringify(data)
        },
        method: 'POST',
        header: {
          'content-type': 'application/x-www-form-urlencoded' //默认值
        },
        dataType: 'json',
        complete() {
          wx.hideNavigationBarLoading();
        },
        success: function ({ data }) {
          if (data.code && data.code == '000000') {
            wx.showToast({
              title: '修改成功！'
            });
            that.setData({
              oldpassword: '',
              password: '',
              rpassword: ''
            })
          } else {
            wx.showToast({
              title: data.msg,
              icon: 'none'
            })
          }
        },
        fail: function (res) {
          wx.showToast({
            title: '网络连接失败',
            icon: 'loading'
          })
        }
      })
    }

  },

  onReady: function () {

  },

  onShow: function () {

  },

  onShareAppMessage: function () {

  }
})