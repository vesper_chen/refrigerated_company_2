var app = getApp();

Page({
  data: {
    name: '',
    mobile: '',
    account: '',
    ip:''
  },

  onLoad: function (options) {
    var that = this;
     
    that.setData({
      name: app.globalData.user.driverName,
      mobile: app.globalData.user.driverPhone1,
      account: app.globalData.user.driverAccount,
      ip: app.globalData.user.driverAccount == 1 ? app.globalData.webhost : ''
    })
  },

  onShow: function () {

  },

  onPullDownRefresh: function () {

  },

  onReachBottom: function () {

  },

  onShareAppMessage: function () {

  }
})