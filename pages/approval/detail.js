// pages/approval/detail.js

const app = getApp()
var webhost = app.globalData.webhost;
var getUser;
var getDetail;
var getCodeList;



Page({

  /**
   * Page initial data
   */
  data: {
    inputShowed: false,
    inputVal: "",
    inputBoxShow: false,
    selectBoxShow: false,
    trBtnDisabled: false,
    approvalDesc: '',
    selectList: [],
    valueList: [],
    // myView: {},
    widgetList:[],
    isEidtable: false,
    isDoing: true,
    isOuterManager: 0
  },

  select: function(e){
    var that = this;
    var widgetId = e.currentTarget.dataset.widgetid;
    var fieldId = e.currentTarget.dataset.fieldid;
    console.log(widgetId)
    var value = +e.detail.value;
    var item = 'widgetList[' + widgetId + '].fieldList[' + fieldId + '].value'
    console.log(item)
    that.setData({
      [item]: value
    })
  },

  inputText: function(e){
    var that = this;
    var inputVal = e.detail.value;
    var widgetid = e.currentTarget.dataset.widgetid;
    var fieldid = e.currentTarget.dataset.fieldid;
    console.log(fieldid)
    var item = 'widgetList[' + widgetid + '].fieldList[' + fieldid +  '].value'
    that.setData({
      [item]:  inputVal
    })
  },

  //自写的带有文本输入框的确认弹窗
  showInputBox: function () {
    this.setData({
      inputBoxShow: true
    });
    this.setData({
      isScroll: false
    });
  },
  invisible: function () {
    this.setData({
      inputBoxShow: false
    });
    this.setData({
      isScroll: true
    });
  },
  selectInvisible: function () {
    this.setData({
      selectBoxShow: false
    });
    this.setData({
      isScroll: true
    });
  },

  stopPageScroll: function () {
    return
  },

  //根据不同操作显示不用的弹窗
  setClick(e) {
    var that = this;
    var actionCode = e.currentTarget.dataset.actioncode;
    var actionName = e.currentTarget.dataset.actionname;
    var workingItem =  app.globalData.workingItem;
    that.setData({
      actionCode: actionCode,
      actionName: actionName,
    })
    if (actionCode == 'turnReview') {
      that.setData({
        selectBoxShow: true
      })
      getUser(workingItem.procTmpId, workingItem.actTmpId);
    } else {
      that.setData({
        inputBoxShow: true
      })
    }
  },

  //输入审批意见时动态改变审批意见的值
  inputDesc(e) {
    this.setData({
      approvalDesc: e.detail.value
    })
  },

  //选择转审人
  chooseUser(e) {
    console.log(e)
    this.setData({
      userIndex: e.detail.value
    })
  },

  //对审批操作执行确认
  submit(e) {
    var that = this;
    var workingItem = app.globalData.workingItem;
    wx.showNavigationBarLoading();
    if (that.data.actionCode == 'approval') {
      var data = [{
        String: {
          sessionid: app.globalData.sessionid,
          workItemId: workingItem.workItemId,
          approvalDesc: that.data.approvalDesc
        }
      }]
      if (that.data.approvalDesc) {
        wx.request({
          url: webhost,
          data: {
            eap_username: app.globalData.account,
            eap_password: app.globalData.password,
            boId: "tspdriver_Tsp2BusinessServiceBO_bo", //调用的bo
            methodName: 'doCompleteWorkItems', //调用的具体方法
            returnType: "json", //返回参数类型
            parameters: JSON.stringify(data)
          },
          header: {
            'content-type': 'application/x-www-form-urlencoded'
          },
          method: 'POST',
          complete() {
            wx.hideNavigationBarLoading();
          },
          success({
            data
          }) {
            if (data.code && data.code == '000000') {
              wx.showToast({
                title: '提交成功',
              })
              setTimeout(function () {
                that.setData({
                  actionCode: '',
                  actionName: '',
                  approvalDesc: '',
                  inputBoxShow: false
                })
                wx.navigateBack({
                  delta: 1
                })
              }, 1500)


            } else if (data.code == '003000') {
              wx.showToast({
                title: data.msg,
                icon: 'none'
              })
              setTimeout(function () {
                wx.redirectTo({
                  url: '../login/login',
                })
              }, 1200);
            } else {
              wx.showToast({
                title: data.msg,
              })
            }
          },
          fail: function (res) {
            wx.showToast({
              title: '网络连接失败',
              icon: 'none'
            })
          }
        })
      } else {
        wx.showToast({
          title: '意见不可为空',
          icon: 'none'
        })
        setTimeout(function () {
          wx.hideToast()
        }, 1500)
      }

    } else if (that.data.actionCode == 'reject') {
      var data = [{
        String: {
          sessionid: app.globalData.sessionid,
          procInstId: workingItem.procInstId,
          workItemId: workingItem.workItemId,
          approvalDesc: that.data.approvalDesc
        }
      }]
      if (that.data.approvalDesc) {
        wx.request({
          url: webhost,
          data: {
            eap_username: app.globalData.account,
            eap_password: app.globalData.password,
            boId: "tspdriver_Tsp2BusinessServiceBO_bo", //调用的bo
            methodName: 'doAbort', //调用的具体方法
            returnType: "json", //返回参数类型
            parameters: JSON.stringify(data)
          },
          header: {
            'content-type': 'application/x-www-form-urlencoded'
          },
          method: 'POST',
          complete() {
            wx.hideNavigationBarLoading();
          },
          success({
            data
          }) {
            if (data.code && data.code == '000000') {
              wx.showToast({
                title: '提交成功',
              })
              setTimeout(function () {
                that.setData({
                  actionCode: '',
                  actionName: '',
                  approvalDesc: '',
                  inputBoxShow: false
                })
                wx.navigateBack({
                  delta: 1
                })
              }, 1500)

            } else if (data.code == '003000') {
              wx.showToast({
                title: data.msg,
                icon: 'none'
              })
              setTimeout(function () {
                wx.redirectTo({
                  url: '../login/login',
                })
              }, 1200);
            } else {
              wx.showToast({
                title: data.msg,
              })
            }
          },
          fail: function (res) {
            wx.showToast({
              title: '网络连接失败',
              icon: 'none'
            })
          }
        })
      } else {
        wx.showToast({
          title: '意见不可为空',
          icon: 'none'
        })
        setTimeout(function () {
          wx.hideToast()
        }, 1500)
      }
    } else if (that.data.actionCode == 'turnReview') {
      var data = [{
        String: {
          sessionid: app.globalData.sessionid,
          workItemId: workingItem.workItemId,
          personID: that.data.userCodeList[that.data.userIndex]
        }
      }]
      wx.request({
        url: webhost,
        data: {
          eap_username: app.globalData.account,
          eap_password: app.globalData.password,
          boId: "tspdriver_Tsp2BusinessServiceBO_bo", //调用的bo
          methodName: 'doReassignWorkItem', //调用的具体方法
          returnType: "json", //返回参数类型
          parameters: JSON.stringify(data)
        },
        header: {
          'content-type': 'application/x-www-form-urlencoded'
        },
        method: 'POST',
        complete() {
          wx.hideNavigationBarLoading();
        },
        success({
          data
        }) {
          if (data.code && data.code == '000000') {
            wx.showToast({
              title: '提交成功',
            })
            setTimeout(function () {
              that.setData({
                actionCode: '',
                actionName: '',
                selectBoxShow: false,
                userIndex: 0,
                userNameList: [],
                userCodeList: []
              })
              wx.navigateBack({
                delta: 1
              })
            }, 1500)

          } else if (data.code == '003000') {
            wx.showToast({
              title: data.msg,
              icon: 'none'
            })
            setTimeout(function () {
              wx.redirectTo({
                url: '../login/login',
              })
            }, 1200);
          } else {
            wx.showToast({
              title: data.msg,
            })
          }
        },
        fail: function (res) {
          wx.showToast({
            title: '网络连接失败',
            icon: 'none'
          })
        }
      })
    }
  },

  cancel() {
    this.setData({
      actionCode: '',
      actionName: '',
      approvalDesc: '',
      inputBoxShow: false,
      selectBoxShow: false
    })
  },

  //提交表单的修改
  update(){
    var that = this;
    var widgetList = that.data.widgetList
    var myView = { widgetList };
    var data = [{
      String:{
        sessionid: app.globalData.sessionid,
        myView: myView
      }
    }]
    wx.showNavigationBarLoading()
    wx.request({
      url: webhost,
      data: {
        eap_username: app.globalData.account,
        eap_password: app.globalData.password,
        boId: "tspdriver_Tsp2BusinessServiceBO_bo", //调用的bo
        methodName: 'updateWorkingItemsDetail', //调用的具体方法
        returnType: "json", //返回参数类型
        parameters: JSON.stringify(data)
      },
      header: {
        'content-type': 'application/x-www-form-urlencoded'
      },
      method: 'POST',
      complete() {
        wx.hideNavigationBarLoading();
      },
      success({ data }) {
        if (data.code && data.code == '000000') {
          wx.showToast({
            title: '提交成功',
            icon:'success'
          })
          setTimeout(function(){
            getDetail()
          },2500)
        } else if (data.code == '003000') {
          wx.showToast({
            title: data.msg,
            icon: 'none'
          })
          setTimeout(function () {
            wx.redirectTo({
              url: '../login/login',
            })
          }, 1200);
        }
      },
      fail: function (res) {
        wx.showToast({
          title: '网络连接失败',
          icon: 'none'
        })
      }
    })
  },

  // 预览图片
  previewImg: function (e) {
    //获取当前图片的下标
    var index = e.currentTarget.dataset.index;
    var widgetId = +e.currentTarget.dataset.widgetid;
    //所有图片
    var widgetList = this.data.widgetList;
    var imgs = widgetList[widgetId].urls;
    wx.previewImage({
          //当前显示图片
          current: imgs[index],
          //所有图片
          urls: imgs
        })
  },

  /**
   * Lifecycle function--Called when page load
   */
  onLoad: function(options) {
    var that = this;
    var busiEntityId = options.busiEntityId;
    var sSheetCode = options.sSheetCode;
    var roleList = app.globalData.user.roleList;
    console.log(roleList)
    for (var i = 0; i < roleList.length; i++){
      if (roleList[i].name == '外调管理员'){
        that.setData({
          isOuterManager: 1
        })
      }
    }
    that.setData({
      isDoing: options.isDoing
    })
    getDetail = function() {
      wx.showNavigationBarLoading();
      var data = [{
        String: {
          sessionid: app.globalData.sessionid,
          busiEntityId: busiEntityId,
          sSheetCode: sSheetCode
        }
      }]
      wx.request({
        url: webhost,
        data: {
          eap_username: app.globalData.account,
          eap_password: app.globalData.password,
          boId: "tspdriver_Tsp2BusinessServiceBO_bo", //调用的bo
          methodName: 'queryBusinessWorkingItemsDetail', //调用的具体方法
          returnType: "json", //返回参数类型
          parameters: JSON.stringify(data)
        },
        header: {
          'content-type': 'application/x-www-form-urlencoded'
        },
        method: 'POST',
        complete() {
          wx.hideNavigationBarLoading();
        },
        success({data}) {
          if (data.code && data.code == '000000') {
            console.log(data)
            var myView = data.myView
            var widgetList = data.myView.widgetList;
            var isEidtable = [];
            if (myView && myView.widgetList.length > 0) {
              that.setData({
                myView: myView,
                widgetList: myView.widgetList
              })
              for (var i = 0; i < widgetList.length; i++ ){
                if (widgetList[i].fieldList && widgetList[i].fieldList.length > 0){
                  for (var j = 0; j < widgetList[i].fieldList.length; j++) {
                    if (widgetList[i].fieldList[j].isEdit == '1') {
                      isEidtable[i] = 1
                    }
                  }
                }
              }
              that.setData({
                isEidtable: isEidtable
              })
            } else {
              console.log('暂无数据')
            }
          } else if (data.code == '003000') {
            wx.showToast({
              title: data.msg,
              icon: 'none'
            })
            setTimeout(function() {
              wx.redirectTo({
                url: '../login/login',
              })
            }, 1200);
          }
        },
        fail: function(res) {
          wx.showToast({
            title: '网络连接失败',
            icon: 'none'
          })
        }
      })
    };

    //获取可转审用户列表
    getUser = function (procTmpId, actTmpId) {
      wx.showNavigationBarLoading();
      var data = [{
        String: {
          sessionid: app.globalData.sessionid,
          procTmpId: procTmpId,
          actTmpId: actTmpId,
        }
      }]
      wx.request({
        url: webhost,
        data: {
          eap_username: app.globalData.account,
          eap_password: app.globalData.password,
          boId: "tspdriver_Tsp2BusinessServiceBO_bo", //调用的bo
          methodName: 'getUserByActTmpIdAndProcDefId', //调用的具体方法
          returnType: "json", //返回参数类型
          parameters: JSON.stringify(data)
        },
        header: {
          'content-type': 'application/x-www-form-urlencoded'
        },
        method: 'POST',
        complete() {
          wx.hideNavigationBarLoading();
        },
        success({
          data
        }) {
          if (data.code && data.code == '000000') {
            var list = data.dataList;
            var userNameList = [];
            var userCodeList = [];
            if (list.length > 0) {
              for (var i = 0; i < list.length; i++) {
                userNameList[i] = list[i].a2;
                userCodeList[i] = list[i].a1
              }
              that.setData({
                userIndex: 0,
                userNameList: userNameList,
                userCodeList: userCodeList
              })
            } else {
              that.setData({
                userIndex: 0,
                userNameList: ['暂无可选转审用户'],
                trBtnDisabled: true
              })
            }
          } else if (data.code == '003000') {
            wx.showToast({
              title: data.msg,
              icon: 'none'
            })
            setTimeout(function () {
              wx.redirectTo({
                url: '../login/login',
              })
            }, 1200);
          }
        },
        fail: function (res) {
          wx.showToast({
            title: '网络连接失败',
            icon: 'none'
          })
        }
      })
    }

    //获取码表通用函数
    getCodeList = function (key, valueList, nameList) {
      wx.showNavigationBarLoading();
      var data = [{
        String: {
          codeListKey: key
        }
      }]
      wx.request({
        url: webhost,
        data: {
          eap_username: app.globalData.account,
          eap_password: app.globalData.password,
          boId: "tspdriver_Tsp2BusinessServiceBO_bo", //调用的bo
          methodName: 'getCodeList', //调用的具体方法
          returnType: "json", //返回参数类型
          parameters: JSON.stringify(data)
        },
        header: {
          'content-type': 'application/x-www-form-urlencoded'
        },
        method: 'POST',
        complete() {
          wx.hideNavigationBarLoading();
        },
        success({ data }) {
          if (data.code && data.code == '000000') {
            var list = data.codeList;
            var code = [];
            var name = [];
            if (list.length > 0) {
              for (var i = 0; i < list.length; i++) {
                code[i] = list[i].codeValue;
                name[i] = list[i].codeName;
              }
            }
            that.setData({
              [valueList]: code,
              [nameList]: name,
            })
          } else if (data.code == '003000') {
            wx.showToast({
              title: data.msg,
              icon: 'none'
            })
            setTimeout(function () {
              wx.redirectTo({
                url: '../login/login',
              })
            }, 1200);
          }
        },
        fail: function (res) {
          wx.showToast({
            title: '网络连接失败',
            icon: 'none'
          })
        }
      })
    };

    getDetail();

    setTimeout(function(){
      getCodeList('comm_isYesOrNo', 'valueList', 'selectList');
    },250)
  },

  /**
   * Lifecycle function--Called when page is initially rendered
   */
  onReady: function() {

  },

  /**
   * Lifecycle function--Called when page show
   */
  onShow: function() {

  },

  /**
   * Lifecycle function--Called when page hide
   */
  onHide: function() {

  },

  /**
   * Lifecycle function--Called when page unload
   */
  onUnload: function() {

  },

  /**
   * Page event handler function--Called when user drop down
   */
  onPullDownRefresh: function() {

  },

  /**
   * Called when page reach bottom
   */
  onReachBottom: function() {

  },

  /**
   * Called when user click on the top right corner to share
   */
  onShareAppMessage: function() {

  }
})