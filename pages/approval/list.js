const app = getApp()
var webhost = app.globalData.webhost;
import Toast from '../Vant/toast/toast';
import utils from '../../utils/date.js';
var listData;
var instance;
var getUser;

Page({
  data: {
    state: '1',
    inputShowed: false,
    inputVal: "",
    inputBoxShow: false,
    selectBoxShow: false,
    trBtnDisabled: false,
    approvalDesc: '',
    active: 0,
    dataList: [],
    userIndex: 0,
    userList: [],
    index: 0,
    page: 1,
    startDate: '',
    endDate: '',
    loading: 0, 
  },
  //searchBar的js方法
  showInput: function() {
    this.setData({
      inputShowed: true
    });
  },
  hideInput: function() {
    var that = this;
    that.setData({
      inputVal: "",
      inputShowed: false,
      dataList: [],
      loading: 0,
      page: 1
    });
    listData(that.data.startDate, that.data.endDate, that.data.inputVal, that.data.state);
  },
  clearInput: function() {
    var that = this;
    that.setData({
      inputVal: "",
      dataList: [],
      loading: 0,
      page: 1
    });
    listData(that.data.startDate, that.data.endDate, that.data.inputVal, that.data.state);
  },
  inputTyping: function(e) {
    this.setData({
      inputVal: e.detail.value
    });
  },
  

  //自写的带有文本输入框的确认弹窗
  showInputBox: function() {
    this.setData({
      inputBoxShow: true
    });
    this.setData({
      isScroll: false
    });
  },
  invisible: function() {
    this.setData({
      // inputBoxShow: false
    });
    this.setData({
      isScroll: true
    });
  },
  selectInvisible: function() {
    this.setData({
      selectBoxShow: false
    });
    this.setData({
      isScroll: true
    });
  },

  stopPageScroll: function () {
    return
  },

  onChange(event) {
    var that = this;
    that.setData({
      active: event.detail.index
    });
    if (event.detail.index == 0){
      that.setData({
        state: '1'
      })
    } else{
      that.setData({
        state: '4'
      })
    }
    that.setData({
      page: 1,
      dataList: []
    })
      listData(that.data.startDate, that.data.endDate, that.data.inputVal, that.data.state )
  },

  doSearch(){
    var that = this;
    that.setData({
      page: 1,
      dataList: []
    })
    listData(that.data.startDate, that.data.endDate, that.data.inputVal, that.data.state)
  },

  //根据不同操作显示不用的弹窗
  setClick(e) {
    var that = this;
    var id = e.currentTarget.dataset.id;
    var actionCode = e.currentTarget.dataset.actioncode;
    var actionName = e.currentTarget.dataset.actionname;
    var procInstId = e.currentTarget.dataset.procinstid;
    that.setData({
      workItemId: id,
      actionCode: actionCode,
      actionName: actionName,
      procInstId: procInstId
    })
    if (actionCode == 'turnReview') {
      var actTmpId = e.currentTarget.dataset.acttmpid;
      var procTmpId = e.currentTarget.dataset.proctmpid;
      that.setData({
        selectBoxShow: true
      })
      getUser(procTmpId, actTmpId);
    } else {
      that.setData({
        inputBoxShow: true
      })
    }
  },

  //输入审批意见时动态改变审批意见的值
  inputDesc(e) {
    this.setData({
      approvalDesc: e.detail.value
    })
  },

  //选择转审人
  chooseUser(e){
    console.log(e)
    this.setData({
      userIndex: e.detail.value
    })
  },

  //对审批操作执行确认
  submit(e) {
    var that = this;
    wx.showNavigationBarLoading();
    if (that.data.actionCode == 'approval') {
      var data = [{
        String: {
          sessionid: app.globalData.sessionid,
          workItemId: that.data.workItemId,
          approvalDesc: that.data.approvalDesc
        }
      }]
      if (that.data.approvalDesc) {
        wx.request({
          url: webhost,
          data: {
            eap_username: app.globalData.account,
            eap_password: app.globalData.password,
            boId: "tspdriver_Tsp2BusinessServiceBO_bo", //调用的bo
            methodName: 'doCompleteWorkItems', //调用的具体方法
            returnType: "json", //返回参数类型
            parameters: JSON.stringify(data)
          },
          header: {
            'content-type': 'application/x-www-form-urlencoded'
          },
          method: 'POST',
          complete() {
            wx.hideNavigationBarLoading();
          },
          success({
            data
          }) {
            if (data.code && data.code == '000000') {
              wx.showToast({
                title: '提交成功',
              })
              setTimeout(function() {
                that.setData({
                  workItemId: '',
                  actionCode: '',
                  actionName: '',
                  approvalDesc: '',
                  inputBoxShow: false
                })
              }, 1500)
              that.setData({
                page: 1,
                dataList: [],
                inputVal: '',
              })
              listData(that.data.startDate, that.data.endDate, that.data.inputVal, that.data.state);
            } else if (data.code == '003000') {
              wx.showToast({
                title: data.msg,
                icon: 'none'
              })
              setTimeout(function() {
                wx.redirectTo({
                  url: '../login/login',
                })
              }, 1200);
            } else {
              wx.showToast({
                title: data.msg,
              })
            }
          },
          fail: function(res) {
            wx.showToast({
              title: '网络连接失败',
              icon: 'none'
            })
          }
        })
      } else {
        wx.showToast({
          title: '意见不可为空',
          icon: 'none'
        })
        setTimeout(function() {
          wx.hideToast()
        }, 1500)
      }

    } else if (that.data.actionCode == 'reject') {
      var data = [{
        String: {
          sessionid: app.globalData.sessionid,
          procInstId: that.data.procInstId,
          workItemId: that.data.workItemId,
          approvalDesc: that.data.approvalDesc
        }
      }]
      if (that.data.approvalDesc) {
        wx.request({
          url: webhost,
          data: {
            eap_username: app.globalData.account,
            eap_password: app.globalData.password,
            boId: "tspdriver_Tsp2BusinessServiceBO_bo", //调用的bo
            methodName: 'doAbort', //调用的具体方法
            returnType: "json", //返回参数类型
            parameters: JSON.stringify(data)
          },
          header: {
            'content-type': 'application/x-www-form-urlencoded'
          },
          method: 'POST',
          complete() {
            wx.hideNavigationBarLoading();
          },
          success({
            data
          }) {
            if (data.code && data.code == '000000') {
              wx.showToast({
                title: '提交成功',
              })
              setTimeout(function() {
                that.setData({
                  workItemId: '',
                  actionCode: '',
                  actionName: '',
                  approvalDesc: '',
                  inputBoxShow: false
                })
              }, 1500)
              that.setData({
                page: 1,
                dataList: [],
                inputVal: '',
              })
              listData(that.data.startDate, that.data.endDate, that.data.inputVal, that.data.state);
            } else if (data.code == '003000') {
              wx.showToast({
                title: data.msg,
                icon: 'none'
              })
              setTimeout(function() {
                wx.redirectTo({
                  url: '../login/login',
                })
              }, 1200);
            } else {
              wx.showToast({
                title: data.msg,
              })
            }
          },
          fail: function(res) {
            wx.showToast({
              title: '网络连接失败',
              icon: 'none'
            })
          }
        })
      } else {
        wx.showToast({
          title: '意见不可为空',
          icon: 'none'
        })
        setTimeout(function() {
          wx.hideToast()
        }, 1500)
      }
    } else if (that.data.actionCode == 'turnReview'){
      var data = [{
        String: {
          sessionid: app.globalData.sessionid,
          workItemId: that.data.workItemId,
          personID: that.data.userCodeList[that.data.userIndex]
        }
      }]
      wx.request({
        url: webhost,
        data: {
          eap_username: app.globalData.account,
          eap_password: app.globalData.password,
          boId: "tspdriver_Tsp2BusinessServiceBO_bo", //调用的bo
          methodName: 'doReassignWorkItem', //调用的具体方法
          returnType: "json", //返回参数类型
          parameters: JSON.stringify(data)
        },
        header: {
          'content-type': 'application/x-www-form-urlencoded'
        },
        method: 'POST',
        complete() {
          wx.hideNavigationBarLoading();
        },
        success({
          data
        }) {
          if (data.code && data.code == '000000') {
            wx.showToast({
              title: '提交成功',
            })
            setTimeout(function () {
              that.setData({
                workItemId: '',
                actionCode: '',
                actionName: '',
                selectBoxShow: false,
                userIndex: 0,
                userNameList: [],
                userCodeList: []

              })
            }, 1500)
            that.setData({
              page: 1,
              dataList: [],
              inputVal: '',
            })
            listData(that.data.startDate, that.data.endDate, that.data.inputVal, that.data.state);
          } else if (data.code == '003000') {
            wx.showToast({
              title: data.msg,
              icon: 'none'
            })
            setTimeout(function () {
              wx.redirectTo({
                url: '../login/login',
              })
            }, 1200);
          } else {
            wx.showToast({
              title: data.msg,
            })
          }
        },
        fail: function (res) {
          wx.showToast({
            title: '网络连接失败',
            icon: 'none'
          })
        }
      })
    }
  },
  
  cancel() {
    this.setData({
      workItemId: '',
      actionCode: '',
      actionName: '',
      approvalDesc: '',
      inputBoxShow: false,
      selectBoxShow: false

    })
  },

  viewClick(e) {
    var busiEntityId = e.currentTarget.dataset.id;
    var sSheetCode = e.currentTarget.dataset.code;
    var workItemcurrentState = e.currentTarget.dataset.state;
    var isDoing;
    if (workItemcurrentState == '运行态'){
      isDoing = 1
    } else if (workItemcurrentState == '完成态'){
      isDoing = 0
    }
    var index = +e.currentTarget.dataset.index;
    app.globalData.workingItem = this.data.dataList[index];
    wx.navigateTo({
      url: './detail?busiEntityId=' + busiEntityId + '&sSheetCode=' + sSheetCode + '&isDoing=' + isDoing
    })
  },

  bindDate: function(e) {
    var that = this;
    var date = e.detail.value;
    that.setData({
      page: 1,
      dataList: [],
      startDate: date
    })
    listData(that.data.startDate, that.data.endDate, that.data.inputVal, that.data.state);
  },

  bindDate2: function(e) {
    var that = this;
    var date = e.detail.value;
    that.setData({
      page: 1,
      dataList: [],
      endDate: date
    })
    listData(that.data.startDate, that.data.endDate, that.data.inputVal, that.data.state);
  },

  onLoad: function() {
    var that = this;
    var myDate = new Date();
    var myDate2 = new Date(myDate.getTime() - 30 * 24 * 3600 * 1000);
    var date = utils.Format('yyyy-MM-dd', myDate);
    var date2 = utils.Format('yyyy-MM-dd', myDate2);
    that.setData({
      startDate: date2,
      endDate: date,
      date: date
    })

    listData = function(start, end, taskName, state) {
      wx.showNavigationBarLoading();
      var data = [{
        String: {
          sessionid: app.globalData.sessionid,
          a1: taskName,
          a2: start,
          a3: end,
          a4: state,
          pageNumber: that.data.page,
          pageSize: 5
        }
      }]
      wx.request({
        url: webhost,
        data: {
          eap_username: app.globalData.account,
          eap_password: app.globalData.password,
          boId: "tspdriver_Tsp2BusinessServiceBO_bo", //调用的bo
          methodName: 'queryWorkingItems', //调用的具体方法
          returnType: "json", //返回参数类型
          parameters: JSON.stringify(data)
        },
        header: {
          'content-type': 'application/x-www-form-urlencoded'
        },
        method: 'POST',
        complete() {
          wx.hideNavigationBarLoading();
        },
        success({
          data
        }) {
          if (data.code && data.code == '000000') {
            var list = data.dataList;
            if (list.length > 0) {
              that.setData({
                loading: 0,
                dataList: that.data.dataList.concat(list)
              })
            } else {
              that.setData({
                loading: 2
              })
            }
          } else if (data.code == '003000') {
            wx.showToast({
              title: data.msg,
              icon: 'none'
            })
            setTimeout(function() {
              wx.redirectTo({
                url: '../login/login',
              })
            }, 1200);
          }
        },
        fail: function(res) {
          wx.showToast({
            title: '网络连接失败',
            icon: 'none'
          })
        }
      })
    }

    //获取可转审用户列表
    getUser = function(procTmpId, actTmpId) {
      wx.showNavigationBarLoading();
      var data = [{
        String: {
          sessionid: app.globalData.sessionid,
          procTmpId: procTmpId,
          actTmpId: actTmpId,
        }
      }]
      wx.request({
        url: webhost,
        data: {
          eap_username: app.globalData.account,
          eap_password: app.globalData.password,
          boId: "tspdriver_Tsp2BusinessServiceBO_bo", //调用的bo
          methodName: 'getUserByActTmpIdAndProcDefId', //调用的具体方法
          returnType: "json", //返回参数类型
          parameters: JSON.stringify(data)
        },
        header: {
          'content-type': 'application/x-www-form-urlencoded'
        },
        method: 'POST',
        complete() {
          wx.hideNavigationBarLoading();
        },
        success({
          data
        }) {
          if (data.code && data.code == '000000') {
            var list = data.dataList;
            var userNameList = [];
            var userCodeList = [];
            if (list.length > 0) {
              for(var i = 0; i < list.length; i++){
                userNameList[i] = list[i].a2;
                userCodeList[i] = list[i].a1
              }
              that.setData({
                userIndex: 0,
                userNameList: userNameList,
                userCodeList: userCodeList
              })
            } else {
              that.setData({
                userIndex: 0,
                userNameList: ['暂无可选转审用户'],
                trBtnDisabled: true
              })
            }
          } else if (data.code == '003000') {
            wx.showToast({
              title: data.msg,
              icon: 'none'
            })
            setTimeout(function () {
              wx.redirectTo({
                url: '../login/login',
              })
            }, 1200);
          }
        },
        fail: function (res) {
          wx.showToast({
            title: '网络连接失败',
            icon: 'none'
          })
        }
      })
    }

    // listData();

  },

  onPullDownRefresh() {
    var that = this;
    setTimeout(function() {
      that.setData({
        page: 1,
        dataList: []
      })
      listData(that.data.startDate, that.data.endDate, that.data.inputVal, that.data.state);
      wx.stopPullDownRefresh()
    }, 2000)
  },

  onShareAppMessage() {

  },

  onShow() {
    var that = this;
    setTimeout(function() {
      that.setData({
        // active: 0,
        page: 1,
        dataList: []
      })
      listData(that.data.startDate, that.data.endDate, that.data.inputVal, that.data.state)
    }, 500)
  },

  onReachBottom() {
    var that = this;
    if (that.data.loading == 0) {
      that.setData({
        loading: true,
        page: that.data.page + 1
      })
      setTimeout(() => {
        listData(that.data.startDate, that.data.endDate, that.data.inputVal, that.data.state);
      }, 300);
    }
  }
})