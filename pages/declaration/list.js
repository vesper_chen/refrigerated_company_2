// pages/declaration/list.js
const app = getApp()
var webhost = app.globalData.webhost;
import Toast from '../Vant/toast/toast';
import utils from '../../utils/date.js';
var listData;
var instance;

Page({
  data: {
    inputShowed: false,
    inputVal: "",
    inputRouteShowed: false,
    inputRouteVal: "",
    inputBoxShow: false,
    selectBoxShow: false,
    approvalDesc: '',
    dataList: [],
    index: 0,
    page: 1,
    startDate: '',
    endDate: '',
    customer: '',
    route: '',
    loading: 0
  },
  //发货客户searchBar的js方法
  showInput: function () {
    this.setData({
      inputShowed: true,
      inputRouteShowed: false
    });
  },
  hideInput: function () {
    this.setData({
      inputVal: "",
      inputShowed: false,
      dataList: [],
      page: 1,
      loading: 1,
    });
    listData();
  },
  clearInput: function () {
    this.setData({
      inputVal: ""
    });
  },
  inputTyping: function (e) {
    this.setData({
      inputVal: e.detail.value
    });
  },

  //路线searchBar的js方法
  showRouteInput: function () {
    this.setData({
      inputRouteShowed: true,
      inputShowed: false,
    });
  },
  hideRouteInput: function () {
    this.setData({
      inputRouteVal: "",
      inputRouteShowed: false,
      dataList: [],
      page: 1,
      loading: 1,
    });
    listData();
  },
  clearRouteInput: function () {
    this.setData({
      inputRouteVal: ""
    });
  },
  inputRouteTyping: function (e) {
    this.setData({
      inputRouteVal: e.detail.value
    });
  },

  //输入查询条件后进行搜索
  doSearch: function () {
    var that = this;
    that.setData({
      page: 1,
      loading: 0,
      // selectedIndex: -1,
      // selectedState: '',
      dataList: []
    })
    listData();
  },

  //自写的带有文本输入框的确认弹窗
  showInputBox: function () {
    this.setData({
      inputBoxShow: true
    });
    this.setData({
      isScroll: false
    });
  },
  invisible: function () {
    this.setData({
      inputBoxShow: false
    });
    this.setData({
      isScroll: true
    });
  },
  selectInvisible: function () {
    this.setData({
      selectBoxShow: false
    });
    this.setData({
      isScroll: true
    });
  },


  viewClick(e) {
    var itemId = e.currentTarget.dataset.itemid;
    wx.navigateTo({
      url: './declarationDetail?itemId=' + itemId
    })
  },

  add:function(){
    wx.navigateTo({
      url: './addDeclaration',
    })
  },

  bindDate: function (e) {
    var that = this;
    var date = e.detail.value;
    that.setData({
      page: 1,
      loading:0,
      dataList: [],
      startDate: date
    })
    listData();
  },

  bindDate2: function (e) {
    var that = this;
    var date = e.detail.value;
    that.setData({
      page: 1,
      loading: 0,
      dataList: [],
      endDate: date
    })
    listData();
  },

  onLoad: function () {
    var that = this;
    var myDate = new Date();
    var myDate2 = new Date(myDate.getTime() - 30 * 24 * 3600 * 1000);
    var date = utils.Format('yyyy-MM-dd', myDate);
    var date2 = utils.Format('yyyy-MM-dd', myDate2);
    that.setData({
      startDate: date2,
      endDate: date,
      date: date
    })

    listData = function () {
      wx.showNavigationBarLoading();
      var data = [{
        String: {
          sessionid: app.globalData.sessionid,
          a1: that.data.inputVal,
          a2: that.data.inputRouteVal,
          a3: that.data.startDate,
          a4: that.data.endDate,
          pageNumber: that.data.page,
          pageSize: 5
        }
      }]
      wx.request({
        url: webhost,
        data: {
          eap_username: app.globalData.account,
          eap_password: app.globalData.password,
          boId: "tspdriver_Tsp2BusinessServiceBO_bo", //调用的bo
          methodName: 'queryDeclarationList', //调用的具体方法
          returnType: "json", //返回参数类型
          parameters: JSON.stringify(data)
        },
        header: {
          'content-type': 'application/x-www-form-urlencoded'
        },
        method: 'POST',
        complete() {
          wx.hideNavigationBarLoading();
        },
        success({
          data
        }) {
          if (data.code && data.code == '000000') {
            console.log(data)
            var list = data.dataList;
            console.log(list)
            if (list.length > 0) {
              console.log('有数据')
              that.setData({
                loading: 0,
                dataList: that.data.dataList.concat(list)
              })
            } else {
              console.log('无数据')
              that.setData({
                loading: 2
              })
            }
          } else if (data.code == '003000') {
            wx.showToast({
              title: data.msg,
              icon: 'none'
            })
            setTimeout(function () {
              wx.redirectTo({
                url: '../login/login',
              })
            }, 1200);
          }
        },
        fail: function (res) {
          wx.showToast({
            title: '网络连接失败',
            icon: 'none'
          })
        }
      })
    }

    // listData();


  },

  onPullDownRefresh() {
    var that = this;
    setTimeout(function () {
      that.setData({
        page: 1,
        loading: 0,
        // inputVal: '',
        // inputRouteVal: '',
        dataList: []
      })
      listData();
      wx.stopPullDownRefresh()
    }, 2000)
  },

  onShareAppMessage() {

  },

  onShow() {
    var that = this;
    setTimeout(function () {
      that.setData({
        page: 1,
        loading:0,
        dataList: []
      })
      listData();
    }, 500)
  },

  onReachBottom() {
    var that = this;
    console.log(that.data.loading);
    if (that.data.loading == 0) {
      that.setData({
        loading: true,
        page: that.data.page + 1
      })
      setTimeout(() => {
        listData();
      }, 300);
    }
  }
})