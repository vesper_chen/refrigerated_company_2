// pages/declaration/addDeliver.js
var app = getApp();
var webhost = app.globalData.webhost;
var util = require('../../utils/util.js');
var utilDate = require('../../utils/date.js');
var customerData;
var saveAddress;

Page({

  /**
   * Page initial data
   */
  data: {
    customerList: [],
    id:'',
    code: '',
    name: '',
    addressDetail: '',
    contact:'',
    phone: '',
    isSave: false,
    loading: 0,
    inputVal:'',
    boxPage: 1,
    pageSize: 10,
    loading: 0,
    t: '',
    showBox: false
  },

  // 浮窗选择框的js方法
  //显示浮窗
  showSearchBox: function (e) {
    var that = this;
    var info = that.data.info;
    if (info == 'receiving'){
      that.setData({
        inputVal: '',
        selectedIndex: -1,
        boxList: [],
        boxPage: 1,
        loading: 0,
        placeHolder: '客户名称',
      })
      customerData();
      that.setData({
        showBox: true,
      })
    }
  },

  //显示输入框
  showInput: function () {
    this.setData({
      inputShowed: true
    });
  },
  //隐藏输入框
  hideInput: function () {
    this.setData({
      inputVal: "",
      selectedIndex: -1,
      boxPage: 1,
      loading: 1,
      boxList: [],
    });
    customerData();
  },
  //清除输入框内容
  clearInput: function () {
    this.setData({
      inputVal: ""
    });
  },
  //输入框中输入内容时记录输入值
  inputTyping: function (e) {
    clearTimeout(this.data.t)
    this.setData({
      inputVal: e.detail.value,
      boxList: [],
      loading: 1,
      boxPage: 1,
      selectedIndex: -1,
    });
    this.setData({
      t: setTimeout(function () {
        customerData();
      }, 500)
    })
  },
  //使用输入框中的值进行搜索
  doSearch: function () {
    var that = this;
    that.setData({
      boxPage: 1,
      boxList: [],
      selectedIndex: -1,
    })
    customerData();
  },
  //显示弹窗时禁止页面滚动
  stopPageScroll: function () {
    return
  },
  //关闭浮窗
  cancel: function () {
    this.setData({
      inputVal: '',
      showBox: false,
      selectedIndex: -1,
    })
  },
  //在浮窗的加载数据中选择需要的一条数据
  select: function (e) {
    var that = this;
    var index = +e.currentTarget.dataset.id;
    that.setData({
      selectedIndex: index
    })
  },
  //对浮窗内的数据列表进行滚动加载更多数据
  more: function () {
    var that = this;
    if (that.data.loading == 0) {
      that.setData({
        loading: true,
        boxPage: that.data.boxPage + 1
      })
      setTimeout(() => {
        customerData();
      }, 300);
    }
  },
  //选择一条数据后，确认选择结果
  submit: function () {
    var that = this;
    var index = that.data.selectedIndex;
    var list = that.data.boxList;
    that.setData({
      customer: list[index],
      id: list[index].a1,
      code: list[index].a2,
      name: list[index].a3
    })
    setTimeout(function () {
      that.setData({
        showBox: false,
        inputVal: '',
        selectedIndex: -1,
      });
    }, 50)
  },



  inputAddressDetail: function(e) {
    var addressDetail = e.detail.value;
    this.setData({
      addressDetail: addressDetail
    })
  },

  inputContact: function(e) {
    var contact = e.detail.value;
    this.setData({
      contact: contact
    })
  },

  inputPhone: function(e) {
    var phone = e.detail.value;
    this.setData({
      phone: phone
    })
  },

  switchChange:function(e){
    this.setData({
      isSave: e.detail.value
    })
  },

  addDelivery:function(){
    var that = this;
    var item = {
      a1:that.data.id,
      a2: that.data.code,
      a3: that.data.name,
      a4: that.data.addressDetail,
      a5: '',
      a6: '',
      a7: that.data.contact,
      a8: that.data.phone,
    }
    console.log(item)
    if (!item.a3){
      wx.showToast({
        title: '请选择所属客户',
        icon: 'none'
      })
    } else if (!item.a4){
      wx.showToast({
        title: '请输入地址详情',
        icon: 'none'
      })
    } else {
      
      //若勾选保存，则调用保存地址接口
      if (that.data.isSave) {
        saveAddress();
      }
      var pages = getCurrentPages(); // 获取页面栈
      var currPage = pages[pages.length - 1]; // 当前页面
      var prevPage = pages[pages.length - 2]; // 上一个页面
      prevPage.setData({
        info: that.data.info,
        addDeliveryItem: item
      })
      prevPage.addDelivery();
      wx.navigateBack({
        delta: 1
      })
    }
    
  },

  cancelAdd: function(){
    wx.navigateBack({
      delta: 1
    })
  },


  /**
   * Lifecycle function--Called when page load
   */
  onLoad: function(options) {
    var that = this;
    var id = options.id;
    var code = options.code;
    var name = options.name;
    var info = options.info;
    var receivingCustomerCode = options.rcCode;
    var receivingCustomerName = options.rcName;
    if(info == 'receiving'){
      wx.setNavigationBarTitle({
        title: '收货点新增',
      })
    }
    that.setData({
      id: id,
      code: code,
      name: name,
      info: info
    })

    saveAddress = function() {
      wx.showNavigationBarLoading();
      var data = [{
        String: {
          sessionid: app.globalData.sessionid,
          a1: that.data.id,
          a2: that.data.code,
          a3: that.data.name,
          a4: that.data.addressDetail,
          a5: that.data.contact,
          a6: that.data.phone,
        }
      }]
      wx.request({
        url: webhost,
        data: {
          eap_username: app.globalData.account,
          eap_password: app.globalData.password,
          boId: "tspdriver_Tsp2BusinessServiceBO_bo", //调用的bo
          methodName: 'saveLogisticsAddress', //调用的具体方法
          returnType: "json", //返回参数类型
          parameters: JSON.stringify(data)
        },
        header: {
          'content-type': 'application/x-www-form-urlencoded'
        },
        method: 'POST',
        complete() {
          wx.hideNavigationBarLoading();
        },
        success({
          data
        }) {
          if (data.code && data.code == '000000') {
            console.log('保存成功')
          } else if (data.code == '003000') {
            wx.showToast({
              title: data.msg,
              icon: 'none'
            })
            setTimeout(function () {
              wx.redirectTo({
                url: '../login/login',
              })
            }, 1200);
          }
        },
        fail: function (res) {
          wx.showToast({
            title: '网络连接失败',
            icon: 'none'
          })
        }
      })
    };

    customerData = function () {
      wx.showNavigationBarLoading();
      var data = [{
        String: {
          sessionid: app.globalData.sessionid,
          a1: that.data.inputVal,
          pageNumber: that.data.boxPage,
          pageSize: 10
        }
      }]
      wx.request({
        url: webhost,
        data: {
          eap_username: app.globalData.account,
          eap_password: app.globalData.password,
          boId: "tspdriver_Tsp2BusinessServiceBO_bo", //调用的bo
          methodName: 'queryReceiptGoodCustomerList', //调用的具体方法
          returnType: "json", //返回参数类型
          parameters: JSON.stringify(data)
        },
        header: {
          'content-type': 'application/x-www-form-urlencoded'
        },
        method: 'POST',
        complete() {
          wx.hideNavigationBarLoading();
        },
        success({ data }) {
          if (data.code && data.code == '000000') {
            var list = data.dataList;
            if (list.length > 0) {
              that.setData({
                loading: 0,
                boxList: that.data.boxList.concat(list)
              })
            } else {
              that.setData({
                loading: 2
              })
            }
          } else if (data.code == '003000') {
            wx.showToast({
              title: data.msg,
              icon: 'none'
            })
            setTimeout(function () {
              wx.redirectTo({
                url: '../login/login',
              })
            }, 1200);
          }
        },
        fail: function (res) {
          wx.showToast({
            title: '网络连接失败',
            icon: 'none'
          })
        }
      })
    };


  },

  /**
   * Lifecycle function--Called when page is initially rendered
   */
  onReady: function() {

  },

  /**
   * Lifecycle function--Called when page show
   */
  onShow: function() {

  },

  /**
   * Lifecycle function--Called when page hide
   */
  onHide: function() {

  },

  /**
   * Lifecycle function--Called when page unload
   */
  onUnload: function() {

  },

  /**
   * Page event handler function--Called when user drop down
   */
  onPullDownRefresh: function() {

  },

  /**
   * Called when page reach bottom
   */
  onReachBottom: function() {

  },

  /**
   * Called when user click on the top right corner to share
   */
  onShareAppMessage: function() {

  }
})