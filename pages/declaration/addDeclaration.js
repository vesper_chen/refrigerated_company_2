var app = getApp();
var webhost = app.globalData.webhost;
var util = require('../../utils/util.js');
var utilDate = require('../../utils/date.js');
var customerData, routeData, unitData, getCodeList, deliveryInfo, receivingInfo;

Page({
  data: {
    createDate:'',
    remark:'',
    deliveryInfo:[],
    receivingInfo:[],
    boxPage: 1,
    pageSize: 10,
    loading: 0,
    t: '',
    route: { a1: '', a2: '', a3: '' },
    customer: { a1: '', a2: '', a3: '' },
    unit: { a1: '', a2: '' },

    addDeliveryItem:{},
    //要求温度码表、被选项次序
    temperatureValueList: [],
    temperatureNameList: [],
    temIndex: 0,
    //单位码表、被选项次序
    chargeUnitNameList: [],
    chargeUnitValueList: [],
    chargeIndexList: [],
    //货物类型码表、被选项次序
    typeIndexList:[],
    //结算方式码表、被选项次序
    methodIndexList: [],
    settelMethodValueList: [],
    settelMethodNameList: [],
    //是否回单码表、被选项次序
    receiptIndexList: [],

    selectDeliveryList: [],
    selectReceivingList: [],
    isShow: false,
    inputShowed:true,
    disabled: false,
    
    timeList: []
    
  },


  // 浮窗选择框的js方法
  //显示浮窗
  showSearchBox: function (e) {
    var that = this;
    var sign = e.currentTarget.dataset.sign;
    that.setData({
      inputVal: '',
      selectedIndex: -1,
      boxList: [],
      boxPage: 1,
      loading: 0,
      sign: sign
    })
    switch (sign) {
      case 'route':
        that.setData({
          placeHolder: '路线名称',
        })
        routeData();
        break;
      case 'customer':
        that.setData({
          placeHolder: '客户名称',
        })
        customerData();
        break;
      case 'unit':
        that.setData({
          placeHolder: '责任小组名称',
        })
        unitData();
        break;
    }
    that.setData({
      showBox: true,
    })
  },
  //显示输入框
  showInput: function () {
    this.setData({
      inputShowed: true
    });
  },
  //隐藏输入框
  hideInput: function () {
    this.setData({
      inputVal: "",
      selectedIndex: -1,
      boxPage: 1,
      loading: 1,
      boxList: [],
    });
    switch (this.data.sign) {
      case 'route':
        routeData();
        break;
      case 'customer':
        customerData();
        break;
      case 'unit':
        unitData();
        break;
    }
  },
  //清除输入框内容
  clearInput: function () {
    this.setData({
      inputVal: ""
    });
  },
  //输入框中输入内容时记录输入值
  inputTyping: function (e) {
    clearTimeout(this.data.t)
    this.setData({
      inputVal: e.detail.value,
      boxList: [],
      loading: 1,
      boxPage: 1,
      selectedIndex: -1,
    });
    switch (this.data.sign) {
      case 'route':
        this.setData({
          t: setTimeout(function () {
            routeData();
          }, 500)
        })
        break;
      case 'customer':
        this.setData({
          t: setTimeout(function () {
            customerData();
          }, 500)
        })
        break;
      case 'unit':
        this.setData({
          t: setTimeout(function () {
            unitData();
          }, 500)
        })
        break;
      case 'driver':
        this.setData({
          t: setTimeout(function () {
            driverData();
          }, 500)
        })
        break;
    }

  },
  //使用输入框中的值进行搜索
  doSearch: function () {
    var that = this;
    that.setData({
      boxPage: 1,
      boxList: [],
      selectedIndex: -1,
    })
    switch (that.data.sign) {
      case 'route':
        routeData();
        break;
      case 'customer':
        customerData();
        break;
      case 'unit':
        unitData();
        break;
    }
  },
  //显示弹窗时禁止页面滚动
  stopPageScroll: function () {
    return
  },
  //关闭浮窗
  cancel: function () {
    this.setData({
      inputVal: '',
      showBox: false,
      selectedIndex: -1,
    })
  },
  //在浮窗的加载数据中选择需要的一条数据
  select: function (e) {
    var that = this;
    var index = +e.currentTarget.dataset.id;
    that.setData({
      selectedIndex: index
    })
  },
  //对浮窗内的数据列表进行滚动加载更多数据
  more: function () {
    var that = this;
    if (that.data.loading == 0) {
      that.setData({
        loading: true,
        boxPage: that.data.boxPage + 1
      })
      setTimeout(() => {
        switch (that.data.sign) {
          case 'route':
            routeData();
            break;
          case 'customer':
            customerData();
            break;
          case 'unit':
            unitData();
            break;
        }
      }, 300);
    }
  },
  //选择一条数据后，确认选择结果
  submit: function () {
    var that = this;
    var index = that.data.selectedIndex;
    var list = that.data.boxList;

    switch (that.data.sign) {
      case 'route':
        that.setData({
          route: list[index]
        })
        break;
      case 'customer':
        that.setData({
          customer: list[index]
        })
        deliveryInfo();
        setTimeout(function(){
          receivingInfo()
        },200)
        break;
      case 'unit':
        that.setData({
          unit: list[index]
        })
        break;
    }
    setTimeout(function () {
      that.setData({
        showBox: false,
        inputVal: '',
        selectedIndex: -1,
      });
    }, 50)
  },


  onPickerChange: function(e){
    var time = e.detail.y_m_d_h_m;
    var kind = e.currentTarget.dataset.kindofinfo;
    var index = +e.currentTarget.dataset.index;
    var item = 'deliveryTimeList[' + index + ']';
    var itemR = 'receivingTimeList[' + index + ']';
    if(kind == 'delivery'){
      this.setData({
        [item]: time
      })
    } else {
      this.setData({
        [itemR]: time
      })
    }
  },

  changeTem:function(e){
    console.log(e)
    var temIndex = e.detail.value;
    this.setData({
      temIndex: temIndex
    })
  },

  inputRemark:function(e){
    this.setData({
      remark: e.detail.value
    })
  },

  inputAmount: function(e){
    var that = this;
    var index = +e.currentTarget.dataset.index;
    var amount = e.detail.value;
    var item = 'amountList[' + index + ']';
    that.setData({
      [item]: amount 
    })
  },

  inputRAmount: function (e) {
    var that = this;
    var index = +e.currentTarget.dataset.index;
    var amount = e.detail.value;
    var item = 'rAmount[' + index + ']';
    that.setData({
      [item]: amount
    })
  },

  inputRprice: function (e) {
    var that = this;
    var index = +e.currentTarget.dataset.index;
    var price = e.detail.value;
    var item = 'rPrice[' + index + ']';
    that.setData({
      [item]: price
    })
  },

  //跳转到新增发货点信息页面，并将当前页面所选择的发货客户信息(id code name)传递过去
  toAddDeliver:function(e){
    var info = e.currentTarget.dataset.info;
    if (info == 'delivery'){
      var id = this.data.customer.a1;
      var code = this.data.customer.a2;
      var name = this.data.customer.a3; 
      if (id) {
        wx.navigateTo({
          url: './addDeliver?id=' + id + '&code=' + code + '&name=' + name + '&info=' + info 
        })
      } else {
        wx.showModal({
          title: '请先选择发货客户',
        })

      }
    } else{
      if (this.data.customer.a1) {
        var id = this.data.receivingInfo[0].a1;
        var code = this.data.receivingInfo[0].a2;
        var name = this.data.receivingInfo[0].a3;
        wx.navigateTo({
          url: './addDeliver?id=' + id + '&code=' + code + '&name=' + name + '&info=' + info
        })
      } else {
        wx.showModal({
          title: '请先选择发货客户',
        })

      }
    }
  },

  //使用新增发货点信息页面传回来的item动态添加到deliveryInfo中
  addDelivery:function(){
    var that = this;
    var curDate = utilDate.getFormatDateYMDHM();
    var item = that.data.addDeliveryItem;
    if (that.data.info == 'delivery'){
      var timeList = that.data.deliveryTimeList;
      var chargeIndexList = that.data.chargeIndexList;
      var typeIndexList = that.data.typeIndexList;
      var info = that.data.deliveryInfo;
      timeList.push(curDate);
      chargeIndexList.push(0);
      typeIndexList.push(0);
      info.push(item);
      that.setData({
        deliveryInfo: info,
        deliveryTimeList: timeList,
        chargeIndexList: chargeIndexList,
        typeIndexList: typeIndexList
      })
    } else if (that.data.info == 'receiving'){
      var timeList = that.data.deliveryTimeList;
      var methodIndexList = that.data.methodIndexList;
      var rTypeIndexList = that.data.rTypeIndexList;
      var receiptIndexList = that.data.receiptIndexList;
      var rAmount = that.data.rAmount;
      var rChargeIndexList = that.data.rChargeIndexList;
      var rPrice = that.data.rPrice;
      var info = that.data.receivingInfo;
      timeList.push(curDate);
      methodIndexList.push(0);
      rTypeIndexList.push(0);
      receiptIndexList.push(0);
      rAmount.push('');
      rChargeIndexList.push(0);
      rPrice.push('');
      info.push(item)
      that.setData({
        receivingInfo: info,
        receivingTimeList: timeList,
        methodIndexList: methodIndexList,
        rTypeIndexList: rTypeIndexList,
        receiptIndexList: receiptIndexList,
        rAmount: rAmount,
        rChargeIndexList: rChargeIndexList,
        rPrice: rPrice,
      })
      
    } else{
      wx.showToast({
        title: '新增失败',
      })
    }
    

    //直接使用下方的方式修改data中的deliveryInfo可修改成功，但是不会重新渲染页面。。。
    // that.data.deliveryInfo.push(that.data.addDeliveryItem)
  },


  //勾选发货点信息列表
  bindDeliveryCheckbox: function (e) {
    var index = +e.currentTarget.dataset.index;
    var selectDeliveryList = this.data.selectDeliveryList;
    selectDeliveryList[index] = !selectDeliveryList[index];
    this.setData({
      selectDeliveryList: selectDeliveryList
    })
  },


  //勾选收获点信息列表
  bindReceivingCheckbox: function (e) {
    var index = +e.currentTarget.dataset.index;
    var selectReceivingList = this.data.selectReceivingList;
    selectReceivingList[index] = !selectReceivingList[index];
    this.setData({
      selectReceivingList: selectReceivingList
    })
  },

  //修改温度要求
  changeMethod:function(e){
    var listIndex = +e.currentTarget.dataset.index;
    var pickerIndex = +e.detail.value;
    var item = 'methodIndexList['+ listIndex + ']'
    this.setData({
      [item]: pickerIndex
    })
  },

  //修改单位
  changeCharge: function (e) {
    var listIndex = +e.currentTarget.dataset.index;
    var pickerIndex = +e.detail.value;
    var item = 'chargeIndexList[' + listIndex + ']'
    this.setData({
      [item]: pickerIndex
    })
  },

  changeRCharge: function (e) {
    var listIndex = +e.currentTarget.dataset.index;
    var pickerIndex = +e.detail.value;
    var item = 'rChargeIndexList[' + listIndex + ']'
    this.setData({
      [item]: pickerIndex
    })
  },

  //修改货物类型
  changeType: function (e) {
    var listIndex = +e.currentTarget.dataset.index;
    var pickerIndex = +e.detail.value;
    var item = 'typeIndexList[' + listIndex + ']'
    this.setData({
      [item]: pickerIndex
    })
  },

  changeRType: function (e) {
    var listIndex = +e.currentTarget.dataset.index;
    var pickerIndex = +e.detail.value;
    var item = 'rTypeIndexList[' + listIndex + ']'
    this.setData({
      [item]: pickerIndex
    })
  },

  //修改是否回单
  changeReceipt: function (e) {
    var listIndex = +e.currentTarget.dataset.index;
    var pickerIndex = +e.detail.value;
    var item = 'receiptIndexList[' + listIndex + ']'
    this.setData({
      [item]: pickerIndex
    })
  },

  ifNullShowToast: function (value, text) {
    if (!value) {
      wx.showToast({
        title: text,
        icon: 'none'
      })
      return false;
    } else {
      return true;
    }
  },

  //提交新建报单操作
  saveDeclaration:function(){
    var that = this;
    wx.showNavigationBarLoading();
    var deliveryInfo = that.data.deliveryInfo;
    var receivingInfo = that.data.receivingInfo;
    var listD = [];
    var listR = [];
    if(deliveryInfo.length > 0){
      for (var i = 0; i < deliveryInfo.length; i++){
        if(that.data.selectDeliveryList[i]){
          var temp = { b1: deliveryInfo[i].a1, b2: deliveryInfo[i].a2, b3: deliveryInfo[i].a3, b4: deliveryInfo[i].a4, b5: that.data.deliveryTimeList[i], b6: that.data.amountList[i], b7: deliveryInfo[i].a7 ? deliveryInfo[i].a7 : '', b8: deliveryInfo[i].a8 ? deliveryInfo[i].a8 : '', b9: that.data.chargeUnitValueList[that.data.chargeIndexList[i]], b10: that.data.typeValueList[that.data.typeIndexList[i]]}
          listD.push(temp);
        }
      }
      console.log(listD)
    }

    if (receivingInfo.length > 0) {
      for (var i = 0; i < receivingInfo.length; i++) {
        if(that.data.selectReceivingList[i]){
          var item = { c1: receivingInfo[i].a1, c2: receivingInfo[i].a2, c3: receivingInfo[i].a3, c4: receivingInfo[i].a4, c5: that.data.settelMethodValueList[that.data.methodIndexList[i]], c6: that.data.receivingTimeList[i], c7: receivingInfo[i].a5, c8: receivingInfo[i].a6, c9: that.data.receiptValueList[that.data.receiptIndexList[i]], c10: that.data.typeValueList[that.data.rTypeIndexList[i]], c11: that.data.rAmount[i], c12: that.data.chargeUnitValueList[that.data.rChargeIndexList[i]], c13: that.data.rPrice[i],}
          listR.push(item);
        }
      }
      console.log(listR)
    }

    if (that.ifNullShowToast(that.data.customer.a3, '请选择发货客户')){
      if (that.ifNullShowToast(that.data.route.a3, '请选择要求路线')) {
        if (that.ifNullShowToast(that.data.unit.a2, '请选择责任小组')){
          if (that.ifNullShowToast(listD.length, '请至少选择一条发货点信息')) {
            var data = [{
              String: {
                sessionid: app.globalData.sessionid,
                a1: that.data.createDate,
                a2: that.data.customer.a1,
                a3: that.data.customer.a2,
                a4: that.data.customer.a3,
                a5: that.data.route.a1,
                a6: that.data.route.a3,
                a7: that.data.temperatureValueList[that.data.temIndex],
                a8: that.data.remark,
                a9: that.data.unit.a1,
                a10: that.data.unit.a2,
                data1: listD,//发货点信息List
                data2: listR,//收货点信息List
              }
            }]
            wx.request({
              url: webhost,
              data: {
                eap_username: app.globalData.account,
                eap_password: app.globalData.password,
                boId: "tspdriver_Tsp2BusinessServiceBO_bo", //调用的bo
                methodName: 'saveTspDeclaration', //调用的具体方法
                returnType: "json", //返回参数类型
                parameters: JSON.stringify(data)
              },
              header: {
                'content-type': 'application/x-www-form-urlencoded'
              },
              method: 'POST',
              complete() {
                wx.hideNavigationBarLoading();
              },
              success({ data }) {
                if (data.code && data.code == '000000') {
                  wx.showToast({
                    title: '报单新建成功',
                  })
                  setTimeout(function () {
                    wx.navigateBack({
                      delta: 1
                    })
                  }, 1500)
                } else if (data.code == '003000') {
                  wx.showToast({
                    title: data.msg,
                    icon: 'none'
                  })
                  setTimeout(function () {
                    wx.redirectTo({
                      url: '../login/login',
                    })
                  }, 1200);
                } else {
                  wx.showToast({
                    title: data.msg,
                    icon: 'none'
                  })
                }
              },
              fail: function (res) {
                wx.showToast({
                  title: '网络连接失败',
                  icon: 'none'
                })
              }
            })
          }
        }
      }
    }
    
  },

  goBackList: function () {
    wx.navigateBack({
      delta: 1
    })
  },

  

  onLoad: function (options) {
    var that = this;
    var curDate = utilDate.getFormatDate();
    that.setData({
      createDate: curDate,
      ['unit.a1']: app.globalData.user.unitId,
      ['unit.a2']: app.globalData.user.unitName,
    })
    customerData = function () {
      wx.showNavigationBarLoading();
      var data = [{
        String: {
          sessionid: app.globalData.sessionid,
          a1: that.data.inputVal,
          pageNumber: that.data.boxPage,
          pageSize: 10
        }
      }]
      wx.request({
        url: webhost,
        data: {
          eap_username: app.globalData.account,
          eap_password: app.globalData.password,
          boId: "tspdriver_Tsp2BusinessServiceBO_bo", //调用的bo
          methodName: 'queryDeliverGoodCustomerList', //调用的具体方法
          returnType: "json", //返回参数类型
          parameters: JSON.stringify(data)
        },
        header: {
          'content-type': 'application/x-www-form-urlencoded'
        },
        method: 'POST',
        complete() {
          wx.hideNavigationBarLoading();
        },
        success({ data }) {
          if (data.code && data.code == '000000') {
            var list = data.dataList;
            if (list.length > 0) {
              that.setData({
                loading: 0,
                boxList: that.data.boxList.concat(list)
              })
            } else {
              that.setData({
                loading: 2
              })
            }
          } else if (data.code == '003000') {
            wx.showToast({
              title: data.msg,
              icon: 'none'
            })
            setTimeout(function () {
              wx.redirectTo({
                url: '../login/login',
              })
            }, 1200);
          }
        },
        fail: function (res) {
          wx.showToast({
            title: '网络连接失败',
            icon: 'none'
          })
        }
      })
    };

    routeData = function () {
      wx.showNavigationBarLoading();
      var data = [{
        String: {
          sessionid: app.globalData.sessionid,
          a1: that.data.inputVal,
          pageNumber: that.data.boxPage,
          pageSize: 10
        }
      }]
      wx.request({
        url: webhost,
        data: {
          eap_username: app.globalData.account,
          eap_password: app.globalData.password,
          boId: "tspdriver_Tsp2BusinessServiceBO_bo", //调用的bo
          methodName: 'queryRoutesList', //调用的具体方法
          returnType: "json", //返回参数类型
          parameters: JSON.stringify(data)
        },
        header: {
          'content-type': 'application/x-www-form-urlencoded'
        },
        method: 'POST',
        complete() {
          wx.hideNavigationBarLoading();
        },
        success({ data }) {
          if (data.code && data.code == '000000') {
            var list = data.dataList;
            if (list.length > 0) {
              that.setData({
                loading: 0,
                boxList: that.data.boxList.concat(list)
              })
            } else {
              that.setData({
                loading: 2
              })
            }
          } else if (data.code == '003000') {
            wx.showToast({
              title: data.msg,
              icon: 'none'
            })
            setTimeout(function () {
              wx.redirectTo({
                url: '../login/login',
              })
            }, 1200);
          }
        },
        fail: function (res) {
          wx.showToast({
            title: '网络连接失败',
            icon: 'none'
          })
        }
      })
    };

    unitData = function () {
      wx.showNavigationBarLoading();
      var data = [{
        String: {
          sessionid: app.globalData.sessionid,
          a1: that.data.inputVal,
          pageNumber: that.data.boxPage,
          pageSize: 10
        }
      }]
      wx.request({
        url: webhost,
        data: {
          eap_username: app.globalData.account,
          eap_password: app.globalData.password,
          boId: "tspdriver_Tsp2BusinessServiceBO_bo", //调用的bo
          methodName: 'getUnit', //调用的具体方法
          returnType: "json", //返回参数类型
          parameters: JSON.stringify(data)
        },
        header: {
          'content-type': 'application/x-www-form-urlencoded'
        },
        method: 'POST',
        complete() {
          wx.hideNavigationBarLoading();
        },
        success({ data }) {
          if (data.code && data.code == '000000') {
            var list = data.list;
            if (list.length > 0) {
              that.setData({
                loading: 0,
                boxList: that.data.boxList.concat(list)
              })
            } else {
              that.setData({
                loading: 2
              })
            }
          } else if (data.code == '003000') {
            wx.showToast({
              title: data.msg,
              icon: 'none'
            })
            setTimeout(function () {
              wx.redirectTo({
                url: '../login/login',
              })
            }, 1200);
          } else {
            wx.showToast({
              title: data.msg,
              icon: 'none'
            })
          }
        },
        fail: function (res) {
          wx.showToast({
            title: '网络连接失败',
            icon: 'none'
          })
        }
      })
    };

    //获取码表通用函数
    getCodeList = function (key, valueList, nameList) {
      wx.showNavigationBarLoading();
      var data = [{
        String: {
          codeListKey: key
        }
      }]
      wx.request({
        url: webhost,
        data: {
          eap_username: app.globalData.account,
          eap_password: app.globalData.password,
          boId: "tspdriver_Tsp2BusinessServiceBO_bo", //调用的bo
          methodName: 'getCodeList', //调用的具体方法
          returnType: "json", //返回参数类型
          parameters: JSON.stringify(data)
        },
        header: {
          'content-type': 'application/x-www-form-urlencoded'
        },
        method: 'POST',
        complete() {
          wx.hideNavigationBarLoading();
        },
        success({ data }) {
          if (data.code && data.code == '000000') {
            var list = data.codeList;
            var code = [];
            var name = [];
            if (list.length > 0) {
              for (var i = 0; i < list.length; i++) {
                code[i] = list[i].codeValue;
                name[i] = list[i].codeName;
              }
            }
            that.setData({
              [valueList]: code,
              [nameList]: name,
            })
          } else if (data.code == '003000') {
            wx.showToast({
              title: data.msg,
              icon: 'none'
            })
            setTimeout(function () {
              wx.redirectTo({
                url: '../login/login',
              })
            }, 1200);
          }
        },
        fail: function (res) {
          wx.showToast({
            title: '网络连接失败',
            icon: 'none'
          })
        }
      })
    };

    //根据选择的发货客户获取发货点信息
    deliveryInfo = function () {
      wx.showNavigationBarLoading();
      var data = [{
        String: {
          sessionid: app.globalData.sessionid,
          a1: that.data.customer.a1,
        }
      }]
      wx.request({
        url: webhost,
        data: {
          eap_username: app.globalData.account,
          eap_password: app.globalData.password,
          boId: "tspdriver_Tsp2BusinessServiceBO_bo", //调用的bo
          methodName: 'queryDeliverGoodCustomerInformationList', //调用的具体方法
          returnType: "json", //返回参数类型
          parameters: JSON.stringify(data)
        },
        header: {
          'content-type': 'application/x-www-form-urlencoded'
        },
        method: 'POST',
        complete() {
          wx.hideNavigationBarLoading();
        },
        success({ data }) {
          if (data.code && data.code == '000000') {
            var curDate = utilDate.getFormatDateYMDHM();
            var timeList = [];
            var amountList = [];
            var chargeIndexList = [];
            var typeIndexList = [];
            var selectDeliveryList = [];
            var list = data.dataList;
            if (list && list.length >= 0) {
              for(var i=0; i<list.length; i++){
                timeList[i] = curDate,
                amountList[i] = '',
                chargeIndexList[i] = 0,
                typeIndexList[i] = 0,
                selectDeliveryList[i] = false
              }
              that.setData({
                deliveryTimeList: timeList,//发货点信息列表对应的初始要求装车时间数组
                amountList: amountList,//发货点信息列表对应的初始数量数组
                chargeIndexList: chargeIndexList, //发货点信息列表对应的单位被选次序数组
                typeIndexList: typeIndexList,
                selectDeliveryList: selectDeliveryList,//发货点信息列表对应的列表被选择数组
                deliveryInfo: list
              })
            } else {
              that.setData({
                deliveryInfo: []
              })
            }
          } else if (data.code == '003000') {
            wx.showToast({
              title: data.msg,
              icon: 'none'
            })
            setTimeout(function () {
              wx.redirectTo({
                url: '../login/login',
              })
            }, 1200);
          }
        },
        fail: function (res) {
          wx.showToast({
            title: '网络连接失败',
            icon: 'none'
          })
        }
      })
    };

    receivingInfo = function () {
      wx.showNavigationBarLoading();
      var data = [{
        String: {
          sessionid: app.globalData.sessionid,
          a1: that.data.customer.a1,
        }
      }]
      wx.request({
        url: webhost,
        data: {
          eap_username: app.globalData.account,
          eap_password: app.globalData.password,
          boId: "tspdriver_Tsp2BusinessServiceBO_bo", //调用的bo
          methodName: 'queryReceivingGoodCustomerInformationList', //调用的具体方法
          returnType: "json", //返回参数类型
          parameters: JSON.stringify(data)
        },
        header: {
          'content-type': 'application/x-www-form-urlencoded'
        },
        method: 'POST',
        complete() {
          wx.hideNavigationBarLoading();
        },
        success({ data }) {
          if (data.code && data.code == '000000') {
            var curDate = utilDate.getFormatDateYMDHM();
            var list = data.dataList;
            var timeList = [];
            var methodIndexList = [];
            var rTypeIndexList = [];
            var receiptIndexList = [];
            var rAmount = [];
            var rChargeIndexList = [];
            var rPrice = [];
            var selectReceivingList =[];
            if (list && list.length >= 0) {
              for(var i=0; i<list.length; i++){
                methodIndexList[i] = 0;
                timeList[i] = curDate;
                rTypeIndexList[i] = 0;
                receiptIndexList[i] = 0;
                rAmount[i] = '';
                rChargeIndexList[i] = 0;
                rPrice[i] = '';
                selectReceivingList[i] = false;
              }
              that.setData({
                receivingInfo: list,
                methodIndexList: methodIndexList,
                receivingTimeList: timeList,
                rTypeIndexList: rTypeIndexList,
                receiptIndexList: receiptIndexList,
                rAmount: rAmount,
                rChargeIndexList: rChargeIndexList,
                rPrice: rPrice,
                selectReceivingList: selectReceivingList
              })
            } else {
              that.setData({
                receivingInfo: []
              })
            }
          } else if (data.code == '003000') {
            wx.showToast({
              title: data.msg,
              icon: 'none'
            })
            setTimeout(function () {
              wx.redirectTo({
                url: '../login/login',
              })
            }, 1200);
          }
        },
        fail: function (res) {
          wx.showToast({
            title: '网络连接失败',
            icon: 'none'
          })
        }
      })
    };

    //获取发货点信息的温度要求码表
    getCodeList('tsp2_tempRequirement', 'temperatureValueList', 'temperatureNameList');

    //获取收货点信息的结算方式码表
    setTimeout(function () {
      getCodeList('tsp2_ modeOfPaymentAr', 'settelMethodValueList', 'settelMethodNameList');
    },250);

    //获取货物类型码表
    setTimeout(function () {
      getCodeList('tsp2_cargoType', 'typeValueList', 'typeNameList');
    }, 500);

    //获取是否回单码表
    setTimeout(function () {
      getCodeList('tsp2_isYesOrNo', 'receiptValueList', 'receiptNameList');
    }, 750);

    //获取收货点信息的结算方式码表
    setTimeout(function () {
      getCodeList('tsp2_chargeUnit', 'chargeUnitValueList', 'chargeUnitNameList');
    }, 1000);

  },

  onShow: function (options) {
  },

  onPullDownRefresh: function () {
    setTimeout(() => {
      wx.stopPullDownRefresh();
    }, 1500);
  },

  onReachBottom: function () {
    // var that = this;
    // if (that.data.loading == 0) {
    //   that.setData({
    //     loading: true,
    //     pageNumber: that.data.pageNumber + 1
    //   })
    //   setTimeout(() => {
    //     that.queryPlanForApplyReim();
    //   }, 300);
    // }
  },


})