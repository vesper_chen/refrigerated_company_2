App({
  onLaunch: function () {
    var that  = this;
    wx.getStorage({
      key: 'xylc_session',
      success: function({data}) {
        console.log(data)
        that.globalData.user = data.session;
        that.globalData.sessionid = data.session.sessionid;
        that.globalData.account = data.session.account;
        that.globalData.name = data.session.name;
      },
    });
    wx.getStorage({
      key: 'xylc_password',
      success: function ({ data }) {
        console.log(data)
        that.globalData.password = data.password;
      },
    })
    console.log(that.globalData)
  },
  
  globalData: {
    user: null,
    //测试地址
    // webhost: 'http://39.108.209.155:8080/framework/ws/rest/',

    //生产正式地址
    // webhost: 'https://tms.fjxylc.com/framework/ws/rest/',

    //测试验收地址
    webhost: 'https://xytms.56shapan.com/framework/ws/rest/',

    //图片上传地址
    upLoadHost: 'https://snap.fjxylc.com',
    account: "",
    name: "",
    unitName: "",
    roleList: [],
    sessionid: "",
    password: '',
    workingItem:{},
  }
})